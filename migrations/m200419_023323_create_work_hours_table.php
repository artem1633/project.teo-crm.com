<?php

use yii\db\Migration;

/**
 * Handles the creation of table `work_hours`.
 */
class m200419_023323_create_work_hours_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('work_hours', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer()->comment('Id задачи'),
            'hours_count' => $this->integer()->comment('Количество часов'),
            'work_done' => $this->string()->comment('Выполнены работы'),
            'comment' => $this->string()->comment('Комментарий'),
            'user_id' => $this->integer()->comment('Автор'),
            'created_at' => $this->dateTime()->comment('Дата и время создания')
        ]);
        $this->createIndex(
            'idx-work_hours-user_id',
            'work_hours',
            'user_id'
        );
        $this->createIndex(
            'idx-work_hours-task_id',
            'work_hours',
            'task_id'
        );
        $this->addForeignKey(
            'fk-work_hours-user_id',
            'work_hours',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-work_hours-task_id',
            'work_hours',
            'task_id',
            'task',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-work_hours-task_id',
            'work_hours'
        );
        $this->dropForeignKey(
            'fk-work_hours-user_id',
            'work_hours'
        );
        $this->dropIndex(
            'idx-work_hours-task_id',
            'work_hours'
        );
        $this->dropIndex(
            'idx-work_hours-user_id',
            'work_hours'
        );
        $this->dropTable('work_hours');
    }
}
