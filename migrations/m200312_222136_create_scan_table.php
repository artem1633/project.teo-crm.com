<?php

use yii\db\Migration;

/**
 * Handles the creation of table `scan`.
 */
class m200312_222136_create_scan_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('scan', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('название'),
            'loaded_at' => $this->dateTime()->comment('дата и время загрузки'),
            'link' => $this->string()->comment('ссылка на файл'),
            'author_id' => $this->integer()->comment('кто загрузил'),
            'user_id' => $this->integer()->comment('кто загрузил'),
            'contract_id' => $this->integer()->comment('id договора'),
            'agreement_id' => $this->integer()->comment('id Д/С'),
            'task_id' => $this->integer()->comment('id Задачи'),
            'order_id' => $this->integer()->comment('id Заказа'),
        ]);
        $this->createIndex(
            'idx-scan-author_id',
            'scan',
            'author_id'
        );
//        $this->createIndex(
//            'idx-scan-contract_id',
//            'scan',
//            'contract_id'
//        );
//        $this->createIndex(
//            'idx-scan-agreement_id',
//            'scan',
//            'agreement_id'
//        );
        $this->addForeignKey(
            'fk-scan-author_id',
            'scan',
            'author_id',
            'user',
            'id',
            'SET NULL'
        );
//        $this->addForeignKey(
//            'fk-scan-contract_id',
//            'scan',
//            'contract_id',
//            'contract',
//            'id',
//            'SET NULL'
//        );
//        $this->addForeignKey(
//            'fk-scan-agreement_id',
//            'scan',
//            'agreement_id',
//            'agreement',
//            'id',
//            'SET NULL'
//        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
//        $this->dropForeignKey(
//            'fk-scan-contract_id',
//            'scan'
//        );
        $this->dropForeignKey(
            'fk-scan-author_id',
            'scan'
        );
//        $this->dropForeignKey(
//            'fk-scan-agreement_id',
//            'scan'
//        );
//        $this->dropIndex(
//            'idx-scan-contract_id',
//            'scan'
//        );
        $this->dropIndex(
            'idx-scan-author_id',
            'scan'
        );
//        $this->dropIndex(
//            'idx-scan-agreement_id',
//            'scan'
//        );

        $this->dropTable('scan');

    }
}
