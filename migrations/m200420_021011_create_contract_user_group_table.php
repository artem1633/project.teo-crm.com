<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contract_user_group`.
 */
class m200420_021011_create_contract_user_group_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contract_user_group', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('id пользователя'),
            'contract_id' => $this->integer()->comment('id проекта')
        ]);
        $this->createIndex(
            'idx-contract_user_group-user_id',
            'contract_user_group',
            'user_id'
        );
        $this->createIndex(
            'idx-contract_user_group-contract_id',
            'contract_user_group',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-contract_user_group-contract_id',
            'contract_user_group',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-contract_user_group-user_id',
            'contract_user_group',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-contract_user_group-user_id',
            'contract_user_group'
        );

        $this->dropForeignKey(
            'fk-contract_user_group-contract_id',
            'contract_user_group'
        );
        $this->dropIndex(
            'idx-contract_user_group-contract_id',
            'contract_user_group'
        );
        $this->dropIndex(
            'idx-contract_user_group-user_id',
            'contract_user_group'
        );
        $this->dropTable('contract_user_group');
    }
}
