<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_user_group`.
 */
class m200420_082705_create_order_user_group_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order_user_group', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('id пользователя'),
            'order_id' => $this->integer()->comment('id заказа')
        ]);
        $this->createIndex(
            'idx-order_user_group-user_id',
            'order_user_group',
            'user_id'
        );
        $this->createIndex(
            'idx-order_user_group-order_id',
            'order_user_group',
            'order_id'
        );
        $this->addForeignKey(
            'fk-order_user_group-order_id',
            'order_user_group',
            'order_id',
            'order',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-order_user_group-user_id',
            'order_user_group',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-order_user_group-user_id',
            'order_user_group'
        );

        $this->dropForeignKey(
            'fk-order_user_group-order_id',
            'order_user_group'
        );
        $this->dropIndex(
            'idx-order_user_group-order_id',
            'order_user_group'
        );
        $this->dropIndex(
            'idx-order_user_group-user_id',
            'order_user_group'
        );
        $this->dropTable('order_user_group');
    }
}
