<?php

use yii\db\Migration;

/**
 * Handles adding is_closed to table `order`.
 */
class m200415_074534_add_is_closed_column_to_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('order', 'is_closed', $this->boolean()->defaultValue(false)->comment('Закрыт заказ'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('order', 'is_closed');
    }
}
