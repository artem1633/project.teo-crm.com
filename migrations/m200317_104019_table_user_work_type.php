<?php

use yii\db\Migration;

/**
 * Class m200317_104019_table_user_work_type
 */
class m200317_104019_table_user_work_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_type', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('id пользователя'),
            'type_id' => $this->integer()->comment('id типа')
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {



        $this->dropTable('user_type');
    }
}
