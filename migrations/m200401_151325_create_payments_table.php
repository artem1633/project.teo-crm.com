<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payments`.
 */
class m200401_151325_create_payments_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('payments', [
            'id' => $this->primaryKey(),
            'comment' => $this->string()->comment('комментарий'),
            'task_id' => $this->integer()->comment('Id задачи'),
            'user_id' => $this->integer()->comment('Автор'),
            'sum'=> $this->string()->comment('Сумма'),
            'created_at' => $this->dateTime()->comment('Дата и время создния')
        ]);
        $this->createIndex(
            'idx-payments-user_id',
            'payments',
            'user_id'
        );
        $this->createIndex(
            'idx-payments-task_id',
            'payments',
            'task_id'
        );
        $this->addForeignKey(
            'fk-payments-user_id',
            'payments',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-payments-task_id',
            'payments',
            'task_id',
            'task',
            'id',
            'SET NULL'
        );


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-payments-task_id',
            'payments'
        );
        $this->dropForeignKey(
            'fk-payments-user_id',
            'payments'
        );
        $this->dropIndex(
            'idx-payments-task_id',
            'payments'
        );
        $this->dropIndex(
            'idx-payments-user_id',
            'payments'
        );
        $this->dropTable('payments');
    }
}
