<?php

use yii\db\Migration;

/**
 * Handles adding workTypes to table `reprot_setting_column`.
 */
class m200412_111324_add_workTypes_column_to_reprot_setting_column_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('report_setting_column', 'order_workTypes', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('report_setting_column', 'order_workTypes');
    }
}
