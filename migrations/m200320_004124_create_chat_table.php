<?php

use yii\db\Migration;

/**
 * Handles the creation of table `chat`.
 */
class m200320_004124_create_chat_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('chat', [
            'id' => $this->primaryKey(),
            'contract_id' => $this->integer()->comment('id договора'),
            'author_id' => $this->integer()->comment('id  автора'),
            'text' => $this->text()->comment('Текст сообщения'),
            'created_at' => $this->dateTime()->comment('дата и время')
        ]);
        $this->createIndex(
            'idx-chat-author_id',
            'chat',
            'author_id'
        );
        $this->addForeignKey(
            'fk-chat-author_id',
            'chat',
            'author_id',
            'user',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-chat-contract_id',
            'chat',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-chat-contract_id',
            'chat',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-chat-author_id',
            'contract'
        );
        $this->dropForeignKey(
            'fk-chat-contract_id',
            'contract'
        );
        $this->dropIndex(
            'idx-chat-contract_id',
            'contract'
        );
        $this->dropIndex(
            'idx-chat-author_id',
            'contract'
        );

        $this->dropTable('chat');
    }
}
