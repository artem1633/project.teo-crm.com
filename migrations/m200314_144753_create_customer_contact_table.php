<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer_contact`.
 */
class m200314_144753_create_customer_contact_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customer_contact', [
            'id' => $this->primaryKey(),
            'name' =>$this->string()->comment('ФИО контактного лица'),
            'position' =>$this->string()->comment('должность контактного лица'),
            'phone' =>$this->string()->comment('телефон контактного лица'),
            'duty_phone' =>$this->string()->comment('служебный тел. контактного лица'),
            'email' =>$this->string()->comment('почта контактного лица'),
            'customer_id' => $this->integer()->comment('котрагент'),
        ]);
        $this->createIndex(
            'idx-customer_contact-customer_id',
            'customer_contact',
            'customer_id'
        );


        $this->addForeignKey(
            'fk-customer_contact-customer_id',
            'customer_contact',
            'customer_id',
            'customer',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-customer_contact-customer_id',
            'customer_contact'
        );

        $this->dropIndex(
            'idx-customer_contact-customer_id',
            'customer_contact'
        );
        $this->dropTable('customer_contact');
    }

}
