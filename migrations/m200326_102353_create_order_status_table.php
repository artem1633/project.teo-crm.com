<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_status`.
 */
class m200326_102353_create_order_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'color' => $this->string()->comment('Цвет')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order_status');
    }
}
