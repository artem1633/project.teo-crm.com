<?php

use yii\db\Migration;

/**
 * Class m200427_143111_change_task_table
 */
class m200427_143111_change_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('task', 'task_desc', $this->text()->comment('Дата и время созданя'));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('task', 'task_desc', $this->string()->comment('Дата и время созданя'));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200427_143111_change_task_table cannot be reverted.\n";

        return false;
    }
    */
}
