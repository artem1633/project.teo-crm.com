<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contract`.
 */
class m200312_222258_create_contract_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contract', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('название договра'),
            'customer_id' => $this->integer()->comment('связь с клиентом'),
            'contract_number' => $this->string()->comment('номер договора'),
            'contract_date' => $this->date()->comment('дата договора'),
            'contract_type' => $this->string()->comment('вид договора'),
            'contract_status' => $this->string()->comment('статус договора'),
            'city_id' => $this->integer()->comment('город договора'),
            'project_name' => $this->string()->comment('название проекта'),
            'contract_deadline' => $this->date()->comment('скрок окончания договора'),
            'post_address' => $this->string()->comment('почтовый адрес'),
            'scan_id' => $this->integer()->comment('сканы документов'),
            'comment' => $this->string()->comment('комментарий'),
            'author_id' => $this->integer()->comment('кто создал'),
            'created_at' => $this->date()->comment('дата созданя'),
            'updated_at' => $this->date()->comment('дата изменения')
        ]);
        $this->createIndex(
            'idx-contract-author_id',
            'contract',
            'author_id'
        );
        $this->addForeignKey(
            'fk-contract-author_id',
            'contract',
            'author_id',
            'user',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-contract-city_id',
            'contract',
            'city_id'
        );
        $this->addForeignKey(
            'fk-contract-city_id',
            'contract',
            'city_id',
            'city',
            'id',
            'SET NULL'
        );
//        $this->createIndex(
//            'idx-contract-work_type_id',
//            'contract',
//            'work_type_id'
//        );
//        $this->addForeignKey(
//            'fk-contract-work_type_id',
//            'contract',
//            'work_type_id',
//            'work_type',
//            'id',
//            'SET NULL'
//        );
        $this->createIndex(
            'idx-contract-customer_id',
            'contract',
            'customer_id'
        );
        $this->addForeignKey(
            'fk-contract-customer_id',
            'contract',
            'customer_id',
            'customer',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-contract-city_id',
            'contract'
        );

//        $this->dropForeignKey(
//            'fk-contract-work_type_id',
//            'contract'
//        );

//        $this->dropIndex(
//            'idx-contract_work_type_id',
//            'contract'
//        );
        $this->dropForeignKey(
            'fk-contract-author_id',
            'contract'
        );
        $this->dropForeignKey(
            'fk-contract-customer_id',
            'contract'
        );
        
        
        $this->dropIndex(
            'idx-contract-city_id',
            'contract'
        );

        $this->dropIndex(
            'idx-contract-author_id',
            'contract'
        );

      

        $this->dropIndex(
            'idx-contract-customer_id',
            'contract'
        );
        $this->dropTable('contract');

    }
}
