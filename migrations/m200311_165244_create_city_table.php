<?php

use yii\db\Migration;

/**
 * Handles the creation of table `city`.
 */
class m200311_165244_create_city_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('city', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);
//        $this->createIndex(
//            'idx-city-user_id',
//            'city',
//            'user_id'
//        );
//        $this->addForeignKey(
//            'fk-city-user_id',
//            'city',
//            'user_id',
//            'user',
//            'id',
//            'SET NULL'
//        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('city');
//        $this->dropForeignKey(
//            'fk-city-user_id',
//            'city'
//        );
//
//        $this->dropIndex(
//            'idx-city-user_id',
//            'city'
//        );
    }
}
