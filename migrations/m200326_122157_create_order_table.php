<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m200326_122157_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название заказа'),
            'hours_plane' => $this->integer()->comment('Часов план'),
            'hours_fact' => $this->string()->comment('Часов факт'),
            'customer_id' => $this->integer()->comment('Наименоване организаци'),
            'author_id' => $this->integer()->comment('Автор'),
            'city_id' => $this->integer()->comment('Город'),
            'order_date' => $this->date()->comment('Дата заказа'),
            'fact_end_date' => $this->date()->comment('Дата фактического закртыия заказа'),
            'contract_id' => $this->integer()->comment('Договор'),
            'order_deadline' => $this->date()->comment('Срок исполнения'),
            'order_status_id' => $this->integer()->comment('Статус заказа'),
            'expired' => $this->boolean()->comment('Просрочен'),
            'scan_added' => $this->boolean()->comment('Добавлен ли скан'),
            'related_orders' => $this->boolean()->comment('Есть ли связанные заказы'),
            'comment' => $this->text()->comment('Комментарии'),
            'order_sum' => $this->string()->comment('Сумма заказа'),
            'created_at' => $this->dateTime()->comment('Дата создания'),
            'updated_at' => $this->dateTime()->comment('Дата изменения')
        ]);
        $this->createIndex(
            'idx-order-order_status_id',
            'order',
            'order_status_id'
        );


        $this->createIndex(
            'idx-order-author_id',
            'order',
            'author_id'
        );
        $this->createIndex(
            'idx-order-customer_id',
            'order',
            'customer_id'
        );
        $this->createIndex(
            'idx-order-city_id',
            'order',
            'city_id'
        );
        $this->createIndex(
            'idx-order-contract_id',
            'order',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-order-order_status_id',
            'order',
            'order_status_id',
            'order_status',
            'id',
            'SET NULL'
        );


        $this->addForeignKey(
            'fk-order-author_id',
            'order',
            'author_id',
            'user',
            'id',
            'SET NULL'
        );

        $this->addForeignKey(
            'fk-order-customer_id',
            'order',
            'customer_id',
            'customer',
            'id',
            'SET NULL'
        );

        $this->addForeignKey(
            'fk-order-city_id',
            'order',
            'city_id',
            'city',
            'id',
            'SET NULL'
        );

        $this->addForeignKey(
            'fk-order-contract_id',
            'order',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-order-order_status_id',
            'order'
        );


        $this->dropForeignKey(
            'fk-order-author_id',
            'order'
        );
        $this->dropForeignKey(
            'fk-order-customer_id',
            'order'
        );
        $this->dropForeignKey(
            'fk-order-contract_id',
            'order'
        );
        $this->dropIndex(
            'idx-order-author_id',
            'order'
        );
        $this->dropIndex(
            'idx-order-customer_id',
            'order'
        );
        $this->dropIndex(
            'idx-order-contract_id',
            'order'
        );
        $this->dropIndex(
            'idx-order-order_status_id',
            'order'
        );


        $this->dropTable('order');
    }
}
