<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m200326_122229_create_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'name' =>$this->string()->comment('Название'),
            'author_id' => $this->integer()->comment('Автор'),
            'order_id' => $this->integer()->comment('Объект Задачи'),
            'task_type' => $this->date()->comment('Тип задачи'),
            'priority' => $this->string()->comment('Приоритет'),
            'task_status_id' => $this->integer()->comment('Статус задачи'),
            'responsible_id' => $this->integer()->comment('Отвественный'),
            'task_deadline' =>$this->date()->comment('Срок Задачи'),
            'checker_id' => $this->integer()->comment('Проверящий'),
            'work_sum' => $this->string()->comment('Стоимость работ'),
            'task_desc' => $this->string()->comment('Описание задачи'),
            'task_rating' => $this->integer()->comment('Оценка'),
            'payment_status' => $this->string()->comment('Статус оплаты'),
            'payment_balance' => $this->string()->comment('Оплачено'),
            'payment_debt' => $this->string()->comment('Остаток оплаты'),
            'review' => $this->integer()->comment('Отзыв о задаче'),
            'hours_plane' => $this->integer()->comment('Кол-во часов план'),
            'hours_fact' => $this->string()->comment('Кол-во часов затрачено'),
            'payment_type' => $this->string()->comment('Тип оплаты'),
            'created_at' => $this->date(),
            'updated_at' => $this->date()
        ]);
        $this->createIndex(
            'idx-task-task_status_id',
            'task',
            'task_status_id'
        );
        $this->createIndex(
            'idx-task-author_id',
            'task',
            'author_id'
        );
        $this->createIndex(
            'idx-task-order_id',
            'task',
            'order_id'
        );
        $this->createIndex(
            'idx-task-responsible_id',
            'task',
            'responsible_id'
        );
        $this->createIndex(
            'idx-task-checker_id',
            'task',
            'checker_id'
        );

        $this->addForeignKey(
            'fk-task-task_status_id',
            'task',
            'task_status_id',
            'task_status',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-task-author_id',
            'task',
            'author_id',
            'user',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-task-order_id',
            'task',
            'order_id',
            'order',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-task-responsible_id',
            'task',
            'responsible_id',
            'user',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-task-checker_id',
            'task',
            'checker_id',
            'user',
            'id',
            'SET NULL'
        );



    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-task-author_id',
            'task'
        );
        $this->dropForeignKey(
            'fk-task-order_id',
            'task'
        );
        $this->dropForeignKey(
            'fk-task-responsible_id',
            'task'
        );
        $this->dropForeignKey(
            'fk-task-checker_id',
            'task'
        );


        $this->dropIndex(
            'idx-task-author_id',
            'task'
        );
        $this->dropIndex(
            'idx-task-order_id',
            'task'
        );
        $this->dropIndex(
            'idx-task-responsible_id',
            'task'
        );
        $this->dropIndex(
            'idx-task-checker_id',
            'task'
        );

        $this->dropTable('task');
    }
}
