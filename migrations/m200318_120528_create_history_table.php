<?php

use yii\db\Migration;

/**
 * Handles the creation of table `history`.
 */
class m200318_120528_create_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('history', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'contract_id' => $this->integer()->comment('Id Договора'),
            'history_comment' => $this->text()->comment('Содержание действия'),
            'created_at' => $this->dateTime(),
        ]);
        $this->createIndex(
            'idx-history-user_id',
            'history',
            'user_id'
        );
        $this->createIndex(
            'idx-history-contract_id',
            'history',
            'contract_id'
        );
        $this->addForeignKey(
            'fk-history-contract_id',
            'history',
            'contract_id',
            'contract',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-history-user_id',
            'history',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-history-user_id',
            'history'
        );

        $this->dropForeignKey(
            'fk-history-contract_id',
            'history'
        );
        $this->dropIndex(
            'idx-history-contract_id',
            'history'
        );
        $this->dropIndex(
            'idx-history-user_id',
            'history'
        );

        $this->dropTable('history');
    }
}
