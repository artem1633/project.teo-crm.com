<?php

use yii\db\Migration;

/**
 * Handles the creation of table `role`.
 */
class m200312_220340_create_role_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('role', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('название'),
            'contract_create' => $this->boolean()->comment('создание договора'),
            'contract_update' => $this->boolean()->comment('изменение  договора'),
            'contract_delete' => $this->boolean()->comment('удаление договора'),
            'contract_view' => $this->boolean()->comment('просмотр договора'),
            'contract_view_all' => $this->boolean()->comment('просмотр всех договорв'),
            'contract_view_sum' => $this->boolean()->comment('просмотр суммы'),
            'contract_view_customer' => $this->boolean()->comment('просмотр контрагента'),
            'order_create' => $this->boolean()->comment('создание заказа'),
            'order_update' => $this->boolean()->comment('изменение заказа'),
            'order_delete' => $this->boolean()->comment('удаление заказа'),
            'order_view' => $this->boolean()->comment('просмотр заказа'),
            'order_view_all' => $this->boolean()->comment('просмотр всех заказов'),
            'order_view_sum' => $this->boolean()->comment('просмотр суммы'),
            'task_create' => $this->boolean()->comment('создание задачи'),
            'task_update' => $this->boolean()->comment('изменение задачи'),
            'task_delete' => $this->boolean()->comment('удаление задачи'),
            'task_view' => $this->boolean()->comment('просмотр задачи'),
            'task_view_all' => $this->boolean()->comment('спросмотр всех задач'),
            'task_sum' => $this->boolean()->comment('просмотр суммы'),
            'task_time' => $this->boolean()->comment('просмотр времени'),
            'task_payment' => $this->boolean()->comment('просмотр платежей'),
            'task_responsible' => $this->boolean()->comment('просмотр ответственного'),
            'directory_access' => $this->boolean()->comment('доступ к справочникам'),
            'user_create' => $this->boolean()->comment('создание пользователей'),
            'user_update' => $this->boolean()->comment('редактирование пользователей'),
            'user_delete' => $this->boolean()->comment('удаление пользователей'),
            'user_view' => $this->boolean()->comment('просмотр пользователей'),
            'user_view_all' => $this->boolean()->comment('просмотр всех пользователей'),
            'report_access' => $this->boolean()->comment('доступ к отчетам'),
        ]);
        $this->insert('role', [
            'name' => 'администратор',
            'contract_create' => true,
            'contract_update' => true,
            'contract_delete' => true,
            'contract_view_all' => true,
            'contract_view' => true,
            'order_create' => true,
            'order_update' => true,
            'order_delete' => true,
            'order_view' => true,
            'order_view_all' => true,
            'task_create' => true,
            'task_update' => true,
            'task_delete' => true,
            'task_view' => true,
            'task_view_all' => true,
            'directory_access' => true,
            'user_create' => true,
            'user_update' => true,
            'user_delete' => true,
            'user_view' => true,
            'user_view_all' => true,
            'report_access' => true,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('role');
    }
}
