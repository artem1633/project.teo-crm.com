<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer_subgroup`.
 */
class m200314_144327_create_customer_subgroup_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customer_subgroup', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('название подгуппы контрагента'),
            'customer_id' => $this->integer()->comment('')
        ]);
        $this->createIndex(
            'idx-customer_subgroup-customer_id',
            'customer_subgroup',
            'customer_id'
        );


        $this->addForeignKey(
            'fk-customer_subgroup-customer_id',
            'customer_subgroup',
            'customer_id',
            'customer',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-customer_subgroup-customer_id',
            'customer_subgroup'
        );

        $this->dropIndex(
            'idx-customer_subgroup-customer_id',
            'customer_subgroup'
        );
        $this->dropTable('customer_subgroup');
    }
}
