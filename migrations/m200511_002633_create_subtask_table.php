<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subtask`.
 */
class m200511_002633_create_subtask_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('subtask', [
            'id' => $this->primaryKey(),
            'desc' => $this->text()->comment('Текст подзадачи'),
            'task_id' => $this->integer()->comment('связь с задачей'),
            'status' => $this->smallInteger()->comment('статус выполнения')
        ]);
        $this->createIndex(
            'idx-subtask-task_id',
            'subtask',
            'task_id'
        );
        $this->addForeignKey(
            'fk-subtask-task_id',
            'subtask',
            'task_id',
            'task',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-subtask-task_id',
            'subtask'
        );
        $this->dropIndex(
            'idx-subtask-task_id',
            'subtask'
        );
        $this->dropTable('subtask');

    }
}
