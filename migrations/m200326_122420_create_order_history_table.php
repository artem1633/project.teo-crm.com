<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_history`.
 */
class m200326_122420_create_order_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order_history', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'order_id' => $this->integer()->comment('Id Заказка'),
            'history_comment' => $this->text()->comment('Содержание действия'),
            'created_at' => $this->dateTime(),
        ]);
        $this->createIndex(
            'idx-order_history-user_id',
            'order_history',
            'user_id'
        );
        $this->createIndex(
            'idx-order_history-order_id',
            'order_history',
            'order_id'
        );
        $this->addForeignKey(
            'fk-order_history-order_id',
            'order_history',
            'order_id',
            'order',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-order_history-user_id',
            'order_history',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-order_history-user_id',
            'order_history'
        );

        $this->dropForeignKey(
            'fk-order_history-order_id',
            'order_history'
        );
        $this->dropIndex(
            'idx-order_history-order_id',
            'order_history'
        );
        $this->dropIndex(
            'idx-order_history-user_id',
            'order_history'
        );
        $this->dropTable('order_history');
    }
}
