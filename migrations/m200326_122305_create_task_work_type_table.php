<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task_work_type`.
 */
class m200326_122305_create_task_work_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task_work_type', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer()->comment('id Задачи'),
            'work_type_id' => $this->integer()->comment('id типов работ')
        ]);
        $this->createIndex(
            'idx-task_work_type-task_id',
            'task_work_type',
            'task_id'
        );
        $this->createIndex(
            'idx-task_work_type-work_type_id',
            'task_work_type',
            'work_type_id'
        );
        $this->addForeignKey(
            'fk-task_work_type-task_id',
            'task_work_type',
            'task_id',
            'task',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-task_work_type-work_type_id',
            'task_work_type',
            'work_type_id',
            'work_type',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-task_work_type-task_id',
            'task_work_type'
        );

        $this->dropForeignKey(
            'fk-task_work_type-work_type_id',
            'task_work_type'
        );
        $this->dropIndex(
            'idx-task_work_type-work_type_id',
            'task_work_type'
        );
        $this->dropIndex(
            'idx-task_work_type-task_id',
            'task_work_type'
        );
        $this->dropTable('task_work_type');
    }
}
