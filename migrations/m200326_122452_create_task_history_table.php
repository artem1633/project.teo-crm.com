<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task_history`.
 */
class m200326_122452_create_task_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task_history', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'task_id' => $this->integer()->comment('Id Задачи'),
            'history_comment' => $this->text()->comment('Содержание действия'),
            'created_at' => $this->dateTime(),
        ]);
        $this->createIndex(
            'idx-task_history-user_id',
            'task_history',
            'user_id'
        );
        $this->createIndex(
            'idx-task_history-task_id',
            'task_history',
            'task_id'
        );
        $this->addForeignKey(
            'fk-task_history-task_id',
            'task_history',
            'task_id',
            'task',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-task_history-user_id',
            'task_history',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-task_history-user_id',
            'task_history'
        );

        $this->dropForeignKey(
            'fk-task_history-task_id',
            'task_history'
        );
        $this->dropIndex(
            'idx-task_history-task_id',
            'task_history'
        );
        $this->dropIndex(
            'idx-task_history-user_id',
            'task_history'
        );
        $this->dropTable('task_history');
    }
}
