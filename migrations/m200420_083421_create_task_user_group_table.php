<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task_user_group`.
 */
class m200420_083421_create_task_user_group_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task_user_group', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('id пользователя'),
            'task_id' => $this->integer()->comment('id задачи')
        ]);
        $this->createIndex(
            'idx-task_user_group-user_id',
            'task_user_group',
            'user_id'
        );
        $this->createIndex(
            'idx-task_user_group-task_id',
            'task_user_group',
            'task_id'
        );
        $this->addForeignKey(
            'fk-task_user_group-task_id',
            'task_user_group',
            'task_id',
            'task',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-task_user_group-user_id',
            'task_user_group',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-task_user_group-user_id',
            'task_user_group'
        );

        $this->dropForeignKey(
            'fk-task_user_group-task_id',
            'task_user_group'
        );
        $this->dropIndex(
            'idx-task_user_group-task_id',
            'task_user_group'
        );
        $this->dropIndex(
            'idx-task_user_group-user_id',
            'task_user_group'
        );
        $this->dropTable('task_user_group');
    }
}
