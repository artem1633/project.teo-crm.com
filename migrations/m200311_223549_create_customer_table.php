<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customers`.
 */
class m200311_223549_create_customer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customer', [
            'id' => $this->primaryKey(),
            'name' =>$this->string()->comment('название организации'),
            'address' =>$this->string()->comment('адрес организации'),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('customer');

    }
}
