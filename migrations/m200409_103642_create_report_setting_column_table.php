<?php

use yii\db\Migration;

/**
 * Handles the creation of table `report_setting_column`.
 */
class m200409_103642_create_report_setting_column_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('report_setting_column', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'contract_name' => $this->boolean()->defaultValue(false),
            'contract_customer_id' => $this->boolean()->defaultValue(false),
            'contract_contract_number' => $this->boolean()->defaultValue(false),
            'contract_contract_date' => $this->boolean()->defaultValue(false),
            'contract_contract_type' => $this->boolean()->defaultValue(false),
            'contract_contract_status' => $this->boolean()->defaultValue(false),
            'contract_city_id' => $this->boolean()->defaultValue(false),
            'contract_project_name' => $this->boolean()->defaultValue(false),
            'contract_contract_deadline' => $this->boolean()->defaultValue(false),
            'contract_contract_sum' => $this->boolean()->defaultValue(false),
            'contract_contract_signature_name' => $this->boolean()->defaultValue(false),
            'contract_contract_executor_name' => $this->boolean()->defaultValue(false),
            'contract_post_address' => $this->boolean()->defaultValue(false),
            'contract_scan_id' => $this->boolean()->defaultValue(false),
            'contract_comment' => $this->boolean()->defaultValue(false),
            'contract_author_id' => $this->boolean()->defaultValue(false),
            'contract_created_at' => $this->boolean()->defaultValue(false),
            'contract_updated_at' => $this->boolean()->defaultValue(false),
            'order_letter_date' => $this->boolean()->defaultValue(false),
            'order_letter_text' => $this->boolean()->defaultValue(false),
            'order_type' => $this->boolean()->defaultValue(false),
            'order_customer_id' => $this->boolean()->defaultValue(false),
            'order_author_id' => $this->boolean()->defaultValue(false),
            'order_city_id' => $this->boolean()->defaultValue(false),
            'order_object_number' => $this->boolean()->defaultValue(false),
            'order_object_address' => $this->boolean()->defaultValue(false),
            'order_order_number' => $this->boolean()->defaultValue(false),
            'order_order_date' => $this->boolean()->defaultValue(false),
            'order_fact_end_date' => $this->boolean()->defaultValue(false),
            'order_contract_id' => $this->boolean()->defaultValue(false),
            'order_order_deadline' => $this->boolean()->defaultValue(false),
            'order_order_status_id' => $this->boolean()->defaultValue(false),
            'order_expired' => $this->boolean()->defaultValue(false),
            'order_scan_added' => $this->boolean()->defaultValue(false),
            'order_related_orders' => $this->boolean()->defaultValue(false),
            'order_comment' => $this->boolean()->defaultValue(false),
            'order_construction_type' => $this->boolean()->defaultValue(false),
            'order_ams_type_id' => $this->boolean()->defaultValue(false),
            'order_placement_id' => $this->boolean()->defaultValue(false),
            'order_order_sum' => $this->boolean()->defaultValue(false),
            'order_created_at' => $this->boolean()->defaultValue(false),
            'order_updated_at' => $this->boolean()->defaultValue(false),
            'order_email_date' => $this->boolean()->defaultValue(false),
            'order_email_matching_date' => $this->boolean()->defaultValue(false),
            'order_edo_date' => $this->boolean()->defaultValue(false),
            'order_edo_getting_date' => $this->boolean()->defaultValue(false),
            'task_name' => $this->boolean()->defaultValue(false),
            'task_author_id' => $this->boolean()->defaultValue(false),
            'task_order_id' => $this->boolean()->defaultValue(false),
            'task_task_type' => $this->boolean()->defaultValue(false),
            'task_priority' => $this->boolean()->defaultValue(false),
            'task_task_status_id' => $this->boolean()->defaultValue(false),
            'task_responsible_id' => $this->boolean()->defaultValue(false),
            'task_task_deadline' => $this->boolean()->defaultValue(false),
            'task_checker_id' => $this->boolean()->defaultValue(false),
            'task_normocontroller_id' => $this->boolean()->defaultValue(false),
            'task_gip_id' => $this->boolean()->defaultValue(false),
            'task_work_sum' => $this->boolean()->defaultValue(false),
            'task_comment' => $this->boolean()->defaultValue(false),
            'task_task_desc' => $this->boolean()->defaultValue(false),
            'task_task_rating' => $this->boolean()->defaultValue(false),
            'task_payment_status' => $this->boolean()->defaultValue(false),
            'task_payment_balance' => $this->boolean()->defaultValue(false),
            'task_payment_debt' => $this->boolean()->defaultValue(false),
            'task_created_at' => $this->boolean()->defaultValue(false),
            'task_updated_at' => $this->boolean()->defaultValue(false),
        ]);

        $this->createIndex(
            'idx-report_setting_column-user_id',
            'report_setting_column',
            'user_id'
        );

        $this->addForeignKey(
            'fk-report_setting_column-user_id',
            'report_setting_column',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-report_setting_column-user_id',
            'report_setting_column'
        );

        $this->dropIndex(
            'idx-report_setting_column-user_id',
            'report_setting_column'
        );

        $this->dropTable('report_setting_column');
    }
}
