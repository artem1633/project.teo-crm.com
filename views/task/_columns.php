<?php

use app\models\Order;
use app\models\TaskStatus;
use app\models\TaskWorkType;
use app\models\User;
use app\models\WorkType;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$model->id]);
        },
//        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order_id',
        'content' => function($data){
            $order = ArrayHelper::getColumn(Order::find()->where(['id' => $data->order_id])->all(), 'name');
            return implode('',$order);

        },
        'filter' => ArrayHelper::map(Order::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'filterInputOptions' => ['class' => 'form-control', 'type' => 'date'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'payment_type',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'hours_fact',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'hours_plane',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'priority',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'workTypes',
        'label' => 'Виды работ',
        'content' => function($model){
            $types = ArrayHelper::getColumn(WorkType::findAll(ArrayHelper::getColumn(TaskWorkType::find()->where(['task_id' => $model->id])->all(), 'work_type_id')), 'name');
            return implode(', ', $types);
        },
        'filter' => ArrayHelper::map(WorkType::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => '', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true,
                'tags' => false,
                'tokenSeparators' => [','],
            ],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'task_status_id',
        'content' => function($data){
            $status = ArrayHelper::getColumn(TaskStatus::find()->where(['id' => $data->task_status_id])->all(), 'name');
            return implode('',$status);

        },
        'filter' => ArrayHelper::map(TaskStatus::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'responsible_id',
        'content' => function($data){
            $user = ArrayHelper::getColumn(User::find()->where(['id' => $data->responsible_id])->all(), 'name');
            return implode('',$user);

        },
        'filter' => ArrayHelper::map(User::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ]
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'task_deadline',
         'filterInputOptions' => ['class' => 'form-control', 'type' => 'date'],
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'checker_id',
         'content' => function($data){
             $user = ArrayHelper::getColumn(User::find()->where(['id' => $data->checker_id])->all(), 'name');
             return implode('',$user);

         },
         'filter' => ArrayHelper::map(User::find()->asArray()->all(), 'id', 'name'),
         'filterType' => GridView::FILTER_SELECT2,
         'filterWidgetOptions' => [
             'options' => ['prompt' => ''],
             'pluginOptions' => ['allowClear' => true],
         ]
     ],

     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'work_sum',
     ],


];   