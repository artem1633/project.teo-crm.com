<?php
use app\models\TaskWorkType;
use app\models\WorkType;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
$task_id = $model->id;
?>
<div class="task-view">
    <div class="row">
    <?php Pjax::begin(['id' => 'pjax-container-info-container', 'enablePushState' => false]); ?>
    <div class="col-md-5">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Информация</h4>
                <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['task/update', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'], ['role' => 'modal-remote', 'class' => 'btn btn-warning btn-xs']) ?>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
            </div>
            <div class="panel-body"  style="height: 450px; overflow-y: auto;">
                <div class="row">
                    <div class="col-md-12">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'name',
                                [
                                    'attribute' => 'responsible_id',
                                    'value' => ArrayHelper::getValue($model->getResponsible()->one(),'name')
                                ],
                                [
                                    'attribute' => 'checker_id',
                                    'value' => ArrayHelper::getValue($model->getChecker()->one(),'name')
                                ],
                                'priority',
                                'task_rating',
                                'task_deadline',
                                'task_desc',
                                [
                                    'attribute' => 'order_id',
                                    'value' => ArrayHelper::getValue($model->getOrder()->one(),'name')
                                ],
                                [
                                    'attribute' =>'workTypes',
                                    'value' => function($data){
                                        $workKinds = ArrayHelper::getColumn(TaskWorkType::find()->where(['task_id' => $data->id ])->all(), 'work_type_id');
                                        $workKinds =ArrayHelper::getColumn(WorkType::find()->where(['id' => $workKinds])->all(), 'name');
                                        $workKinds = implode(', ', $workKinds);
                                        return $workKinds;
                                    }
                                ],
                                [
                                    'attribute' => 'task_status_id',
                                    'value' => ArrayHelper::getValue($model->getTaskStatus()->one(),'name')
                                ],
                                [
                                    'attribute' => 'work_sum',
                                    'visible' => Yii::$app->user->identity->can('task_sum'),
                                ],
                                'payment_status',
                                'payment_type'

                            ]
                        ]) ?>
                    </div>

                </div>
            </div>


        </div>









</div>

    <div class="col-md-7">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Подзадачи
                    </h4>

                    <div class="panel-heading-btn" style="margin-top: -20px;">
                        <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning"
                           data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php Pjax::begin([
                                'id' => 'subtask-view-container-pjax',
                                'enablePushState' => false,
                            ]) ?>
                            <?= $this->render('@app/views/subtask/index', [
                                'dataProvider' => $subtaskDataProvider,
                                'model' => $subtask_model,
                                'task_id' => $model->id,
                            ]) ?>
                            <?php Pjax::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


            <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Часы работ</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                        <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['work-hours/create', 'task_id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'], ['role' => 'modal-remote', 'class' => 'btn btn-warning btn-xs']) ?>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>




                <div class="panel-body"  style="height: 500px; overflow-y: auto;height: 200px;display: none">
                    <?= $this->render('@app/views/work-hours/index', [
                        'searchModel' => $workHoursSearchModel,
                        'dataProvider' => $workHoursDataProvider,
                        'task_id' => $model->id,
                    ]) ?>

                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Платежи</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                        <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['payments/create', 'task_id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'], ['role' => 'modal-remote', 'class' => 'btn btn-warning btn-xs']) ?>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>




                <div class="panel-body"  style="height: 500px; overflow-y: auto;height: 200px;display: none">
                    <?= $this->render('@app/views/payments/index', [
                        'searchModel' => $paymentsSearchModel,
                        'dataProvider' => $paymentsDataProvider,
                        'task_id' => $model->id,
                    ]) ?>

                </div>
            </div>

        </div>

    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Документы</h4>
                <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                    <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['scan/create', 'task_id' => $task_id, 'containerPjaxReload' => '#pjax-container-info-container'],
                        ['role'=>'modal-remote','title'=> 'Добавить документ','class'=>'btn btn-warning btn-xs'])?>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
            </div>


            <div class="panel-body" style="height: 500px; overflow-y: auto;display: none">
                <div class="col-md-12">
                    <?= $this->render('@app/views/task/scan_index', [
                        'searchModel' => $scanSearchModel,
                        'dataProvider' => $scanDataProvider,
                        'task_id' => $model->id,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div id='work_chat_mes' class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Чат</h4>
                <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
            </div>
            <div class="panel-body" id="messageList" style="height: 500px; overflow-y: auto;display: none; scroll;">
                <?php echo \rmrevin\yii\module\Comments\widgets\CommentListWidget::widget(['entity' => (string) 'task-'.$model->id,]);?>
            </div>

        </div>
    </div>
    <div class="col-md-12">
        <div class="history-view">

            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">История изменений</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body"  style="display:none;height: 500px; overflow-y: auto;"">
                <?= $this->render('@app/views/task-history/index', [
                    'searchModel' => $taskHistorySearchModel,
                    'dataProvider' => $taskHistoryDataProvider,
                    'task_id' => $model->id,
                ]) ?>

            </div>
        </div>
    </div>
    </div>




    </div>

</div>

</div>





<script type="text/javascript">
        var div = $("#messageList");
        div.scrollTop(div.prop('scrollHeight'));

    </script>
    <?php
      $this->registerJs(
        '$("document").ready(function(){
         $("#form-pjax-container").on("pjax:end", function() {
            $.pjax.reload({container:"#chat-pjax-container"});  //Reload GridView
            return
         });
    });'
    );

    ?>
<?php Pjax::end() ?>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'fade modal-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>



