<?php

use app\models\Order;
use app\models\TaskStatus;
use app\models\TaskWorkType;
use app\models\User;
use app\models\WorkType;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord == false) {
    $model->workTypes = ArrayHelper::getColumn(TaskWorkType::find()->where(['task_id' => $model->id])->all(), 'work_type_id');
    $model->workTypes = ArrayHelper::getColumn(WorkType::find()->where(['id' => $model->workTypes])->all(), 'id');
}

$orderHidden = false;

if($order_id != null){
    $orderHidden = true;
}

if($model->isNewRecord == false){
    $model->userGroup = ArrayHelper::getColumn(\app\models\TaskUserGroup::find()->where(['task_id' => $model->id])->all(), 'user_id');
    $model->userGroup = ArrayHelper::getColumn(User::find()->where(['id' => $model->userGroup])->all(), 'id');
}


?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="task-form">


    <?= $form->field($model, 'author_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false)?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'task_status_id')->dropDownList(ArrayHelper::map(TaskStatus::find()->orderBy('name asc')->all(), 'id', 'name')) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'priority')->dropDownList([
                'Высокий' => 'Высокий',
                'Средний' => 'Средний',
                'Низкий' => 'Низкий',
            ]) ?>
        </div>
        <div class="col-md-3<?=($orderHidden ? ' hidden' : '')?>">
            <?= $form->field($model, 'order_id')->dropDownList(ArrayHelper::map(Order::find()->all(), 'id', 'name')) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'task_deadline')->input('date') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'hours_plane')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'work_sum')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'payment_type')->dropDownList([
                'По часовая' => 'По часовая',
                'Фиксированная' => 'Фиксированная',
            ]);
            ?>
        </div>
    </div>
    <div class="row">

        <div class="col-md-4">
            <?= $form->field($model, 'responsible_id')->dropDownList(ArrayHelper::map(User::find()->all(), 'id', 'name')) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'checker_id')->dropDownList(ArrayHelper::map(User::find()->all(), 'id', 'name')) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'userGroup')->widget(\kartik\select2\Select2::class, [
                'data' => ArrayHelper::map(User::find()->all(), 'id', 'name'),
                'options' => [
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'tags' => false,
                    'tokenSeparators' => [','],
                ],
            ]) ?>
        </div>
    </div>
    </div>
    <div class="row">

        <div class="col-md-3 hidden">
            <?= $form->field($model, 'task_rating')->dropDownList([
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                ]);
             ?>
        </div>
        <div class="col-md-3 hidden">
            <?= $form->field($model, 'review')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-3 hidden">
            <?= $form->field($model, 'payment_status')->dropDownList([
                'Не оплачено' => 'Не оплачено',
                'Частично оплачено' => 'Частично оплачено',
                'Оплачено' => 'Оплачено',

            ]) ?>
        </div>


    </div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'task_desc')->textarea(['rows' => 6]) ?>
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'listFile')->widget(MultipleInput::className(), [

                'id' => 'my_id',
                'min' => 0,
                'columns' => [
                    [
                        'name' => 'id',
                        'options' => [
                            'type' => 'hidden'
                        ]
                    ],
                    [
                        'name' => 'name',
                        'title' => 'Название',
                    ],
                    [
                        'name' => 'file_new',
                        'title' => 'Файл',
                        'type'  => 'fileInput',
                        'options' => [
                            'pluginOptions' => [
                                'initialPreview'=>[
                                    //add url here from current attribute
                                ],
                                'showPreview' => false,
                                'showCaption' => true,
                                'showRemove' => true,
                                'showUpload' => false,
                            ]
                        ]
                    ],

                ],
            ])->label(false) ?>
        </div>
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
