<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
?>
<div class="task-update">

    <?= $this->render('_form', [
        'model' => $model,
        'order_id' => $order_id,
    ]) ?>

</div>
