<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */
?>
<div class="payments-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'comment',
            'task_id',
            'user_id',
            'sum',
            'created_at',
        ],
    ]) ?>

</div>
