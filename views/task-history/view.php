<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TaskHistory */
?>
<div class="task-history-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'task_id',
            'history_comment:ntext',
            'created_at',
        ],
    ]) ?>

</div>
