<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaskHistory */
?>
<div class="task-history-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
