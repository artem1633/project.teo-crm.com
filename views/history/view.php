<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\History */
?>
<div class="history-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'contract_id',
            'history_comment',
            'created_at',
        ],
    ]) ?>

</div>
