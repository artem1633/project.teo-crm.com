<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WorkType */
?>
<div class="work-type-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
