<?php

use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ReportSettingColumn;
use yii\helpers\Html;
use yii\bootstrap\Modal;

/** @var $columns array */

\johnitvn\ajaxcrud\CrudAsset::register($this);

$this->title = 'Отчет';

$data = [];

$query = ReportSettingColumn::find();

if(Yii::$app->user->identity->isSuperAdmin() == false){
    $query->andWhere(['user_id' => Yii::$app->user->getId()]);
}

$data = ArrayHelper::map($query->all(), 'id', 'name');

$reportColumn = new \app\components\ReportColumn(['columns' => $columns]);
$columns = $reportColumn->getGridColumns();

?>

<div class="panel panel-inverse position-index">
    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title">Отчет</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <?php $form = ActiveForm::begin(['id' => 'search-form', 'method' => 'GET']) ?>

                    <div class="row">
                        <div class="col-md-10">
                            <?= $form->field($searchModel, 'setting')->widget(\kartik\select2\Select2::class, [
                                'data' => $data,
                            ]) ?>
                        </div>
                        <div class="col-md-2">
                            <?= Html::a('<i class="fa fa-plus"></i>', ['report-setting-column/create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote', 'style' => 'margin-top: 22px;']) ?>
                            <?= Html::a('<i class="fa fa-pencil"></i>', ['report-setting-column/update', 'id' => $searchModel->setting], ['class' => 'btn btn-primary', 'role' => 'modal-remote', 'style' => 'margin-top: 22px;']) ?>
                            <?= Html::submitButton('Применить', ['class' => 'btn btn-success', 'style' => 'margin-top: 22px;']) ?>
                        </div>
                    </div>

                <?php ActiveForm::end() ?>
            </div>
        </div>
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'columns' => $columns,
                'panelBeforeTemplate' => '',
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'headingOptions' => ['style' => 'display: none;'],
                    'after'=>'',
                ]
            ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'fade modal-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
