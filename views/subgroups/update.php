<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subgroups */
?>
<div class="subgroups-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
