<?php

use app\models\City;
use app\models\Role;
use app\models\Subgroups;
use app\models\UserKind;
use app\models\UserToCity;
use app\models\UserType;
use app\models\WorkKind;
use app\models\WorkType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord == false) {
    $b = ArrayHelper::getColumn(UserType::find()->where(['user_id' => $model->id])->all(), 'type_id');
    $model->user_type = ArrayHelper::getColumn(WorkType::find()->where(['id' => $b])->all(), 'id');

}
?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<div class="user-form">
    <div class="modal-teo">

        <div class="row">
            <div class="col-md-4">
                <div class="avatar" style="height: 400px;background-image:url(avatars/avatar.png);background-position: center;"></div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-8">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'type')->dropDownList([
                            '0' => 'Штатный',
                            '1' => 'Внештатный',
                            '2' => 'Подрядчик',
                        ],['id'=>'type', 'onChange'=>'$(document).ready(function() {
                        if ( document.getElementById("type").value == "0") {
                            document.getElementById("in").hidden=true; 
                            document.getElementById("out").hidden=true; 
                        }
                        if ( document.getElementById("type").value == "1") {
                            document.getElementById("in").hidden=false; 
                            document.getElementById("out").hidden=true; 
                        }
                        if ( document.getElementById("type").value == "2") {
                            document.getElementById("in").hidden=false; 
                            document.getElementById("out").hidden=false; 
                        }   
                    });']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'login')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'password')->passwordInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, [
                            'mask' => '+7 999 999-99-99',
                        ]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'birth_date')->input('date') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(City::find()->orderBy('name asc')->all(), 'id', 'name')) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'role_id')->dropDownList(ArrayHelper::map(Role::find()->orderBy('name asc')->all(), 'id', 'name')) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'hour_cost')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'experience_rating')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'year_experience')->textInput() ?>
                    </div>
                </div>
                <div class="row" id="in" <?php if ($model->type == 0) echo 'hidden'?>>
                    <div class="col-md-6">
                        <?= $form->field($model, 'user_type')->widget(\kartik\select2\Select2::className(), [
                            'data' => ArrayHelper::map(WorkType::find()->all(), 'id', 'name'),
                            'options' => [
                                'multiple' => true,
                            ],
                            'pluginOptions' => [
                                'tags' => false,
                                'tokenSeparators' => [','],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                            $.ajax({
                                method: 'GET',
                                url: '/user/search-kind?q='+$(this).val().join(','),
                                success: function(response){

                                    var data = '';

                                    $.each(response, function(key, value, i){
                                        
                                        data += '<optgroup label=\''+key+'\'>';
                                    
                                        $.each(value, function(key, value, i){
                                        
                                            data += '<optgroup label=\'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+key+'\'>';
                                        
                                            $.each(value, function(key, value, i){
                                        
                                                data += '<option value=\''+key+'\'>';
                                                    data += value;
                                                data += '</option>';
                                            });
    
                                            data += '</optgroup>';
                                        });

                                        data += '</optgroup>';
                                    });
                                    
                                    $('#user-user_kind').html(data);
                                }
                            });
                        }",
                            ],
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'inn')->textInput() ?>
                    </div>
                </div>
                <div class="row" id="out" <?php if ($model->type == 0 or $model->type == 1) echo 'hidden'?>>
                    <div class="col-md-6">
                        <?= $form->field($model, 'address')->textInput() ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'of_name')->textInput() ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'file')->fileInput() ?>
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'listFile')->widget(MultipleInput::className(), [

                    'id' => 'my_id',
                    'min' => 0,
                    'columns' => [
                        [
                            'name' => 'id',
                            'options' => [
                                'type' => 'hidden'
                            ]
                        ],
                        [
                            'name' => 'name',
                            'title' => 'Название',
                            'enableError' => true,
                        ],
                        [
                            'name' => 'file_new',
                            'title' => 'Файл',
                            'enableError' => true,
                            'type'  => 'fileInput',
                            'options' => [
                                'pluginOptions' => [
                                    'initialPreview'=>[
                                        //add url here from current attribute
                                    ],
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'showRemove' => true,
                                    'showUpload' => false,
                                ]
                            ]
                        ],

                    ],
                ])->label(false) ?>
            </div>
        </div>



        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

    </div>


    <?php ActiveForm::end(); ?>

    <?php if($model->isNewRecord == false): ?>

        <?php

        $script = <<< JS

                            var userKind = $('#user-user_kind').val();


                            $.ajax({
                                method: 'GET',
                                url: '/user/search-kind?q='+$('#user-user_type').val().join(','),
                                success: function(response){

                                    var data = '';

                                    $.each(response, function(key, value, i){
                                        
                                        data += '<optgroup label=\''+key+'\'>';
                                    
                                        $.each(value, function(key, value, i){
                                        
                                            data += '<optgroup label=\''+key+'\'>';
                                        
                                            $.each(value, function(key, value, i){
                                        
                                                data += '<option value=\''+key+'\'>';
                                                    data += value;
                                                data += '</option>';
                                            });
    
                                            data += '</optgroup>';
                                        });

                                        data += '</optgroup>';
                                    });
                                    
                                    $('#user-user_kind').html(data);
                                    
                                     $('#user-user_kind').val(userKind).trigger('change');
                                
                                }
                            });
                            
                            
JS;

        $this->registerJs($script, \yii\web\View::POS_READY);

        ?>

    <?php endif; ?>
