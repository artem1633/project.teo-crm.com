<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $candidateId integer */

CrudAsset::register($this);

?>
<?=GridView::widget([
    'id'=>'crud-order-datatable',
    'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
    'pjax'=>true,
    'columns' => require(__DIR__.'/_order_columns.php'),
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
//    'panel' => [
//        'headingOptions' => ['style' => 'display: none;'],
//        'after'=>'',
//    ]
])?>