<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReportSettingColumn */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-setting-column-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h4>Договоры</h4>
            <hr>
            <?= $form->field($model, 'contract_name')->checkbox() ?>

            <?= $form->field($model, 'contract_customer_id')->checkbox() ?>

            <?= $form->field($model, 'contract_contract_number')->checkbox() ?>

            <?= $form->field($model, 'contract_contract_date')->checkbox() ?>

            <?= $form->field($model, 'contract_contract_type')->checkbox() ?>

            <?= $form->field($model, 'contract_contract_status')->checkbox() ?>

            <?= $form->field($model, 'contract_city_id')->checkbox() ?>

            <?= $form->field($model, 'contract_project_name')->checkbox() ?>

            <?= $form->field($model, 'contract_contract_deadline')->checkbox() ?>

            <?= $form->field($model, 'contract_contract_sum')->checkbox() ?>

            <?= $form->field($model, 'contract_contract_signature_name')->checkbox() ?>

            <?= $form->field($model, 'contract_contract_executor_name')->checkbox() ?>

            <?= $form->field($model, 'contract_post_address')->checkbox() ?>

            <?= $form->field($model, 'contract_comment')->checkbox() ?>

            <?= $form->field($model, 'contract_author_id')->checkbox() ?>

            <?= $form->field($model, 'contract_created_at')->checkbox() ?>

            <?= $form->field($model, 'contract_updated_at')->checkbox() ?>
        </div>
        <div class="col-md-4">
            <h4>Заказы</h4>
            <hr>
            <?= $form->field($model, 'order_letter_date')->checkbox() ?>

            <?= $form->field($model, 'order_letter_text')->checkbox() ?>

            <?= $form->field($model, 'order_type')->checkbox() ?>

            <?= $form->field($model, 'order_customer_id')->checkbox() ?>

            <?= $form->field($model, 'order_author_id')->checkbox() ?>

            <?= $form->field($model, 'order_city_id')->checkbox() ?>

            <?= $form->field($model, 'order_object_number')->checkbox() ?>

            <?= $form->field($model, 'order_object_address')->checkbox() ?>

            <?= $form->field($model, 'order_order_number')->checkbox() ?>

            <?= $form->field($model, 'order_order_date')->checkbox() ?>

            <?= $form->field($model, 'order_fact_end_date')->checkbox() ?>

            <?= $form->field($model, 'order_contract_id')->checkbox() ?>

            <?= $form->field($model, 'order_order_deadline')->checkbox() ?>

            <?= $form->field($model, 'order_order_status_id')->checkbox() ?>

            <?= $form->field($model, 'order_expired')->checkbox() ?>

            <?= $form->field($model, 'order_scan_added')->checkbox() ?>

            <?= $form->field($model, 'order_related_orders')->checkbox() ?>

            <?= $form->field($model, 'order_comment')->checkbox() ?>

            <?= $form->field($model, 'order_construction_type')->checkbox() ?>

            <?= $form->field($model, 'order_ams_type_id')->checkbox() ?>

            <?= $form->field($model, 'order_placement_id')->checkbox() ?>

            <?= $form->field($model, 'order_order_sum')->checkbox() ?>

            <?= $form->field($model, 'order_created_at')->checkbox() ?>

            <?= $form->field($model, 'order_updated_at')->checkbox() ?>

            <?= $form->field($model, 'order_email_date')->checkbox() ?>

            <?= $form->field($model, 'order_email_matching_date')->checkbox() ?>

            <?= $form->field($model, 'order_edo_date')->checkbox() ?>

            <?= $form->field($model, 'order_edo_getting_date')->checkbox() ?>

            <?= $form->field($model, 'order_workTypes')->checkbox() ?>
        </div>
        <div class="col-md-4">
            <h4>Задачи</h4>
            <hr>
            <?= $form->field($model, 'task_author_id')->checkbox() ?>

            <?= $form->field($model, 'task_order_id')->checkbox() ?>

            <?= $form->field($model, 'task_task_type')->checkbox() ?>

            <?= $form->field($model, 'task_priority')->checkbox() ?>

            <?= $form->field($model, 'task_task_status_id')->checkbox() ?>

            <?= $form->field($model, 'task_responsible_id')->checkbox() ?>

            <?= $form->field($model, 'task_task_deadline')->checkbox() ?>

            <?= $form->field($model, 'task_checker_id')->checkbox() ?>

            <?= $form->field($model, 'task_normocontroller_id')->checkbox() ?>

            <?= $form->field($model, 'task_gip_id')->checkbox() ?>

            <?= $form->field($model, 'task_work_sum')->checkbox() ?>

            <?= $form->field($model, 'task_comment')->checkbox() ?>

            <?= $form->field($model, 'task_task_desc')->checkbox() ?>

            <?= $form->field($model, 'task_task_rating')->checkbox() ?>

            <?= $form->field($model, 'task_payment_status')->checkbox() ?>

            <?= $form->field($model, 'task_payment_balance')->checkbox() ?>

            <?= $form->field($model, 'task_payment_debt')->checkbox() ?>

            <?= $form->field($model, 'task_created_at')->checkbox() ?>

            <?= $form->field($model, 'task_updated_at')->checkbox() ?>
        </div>
    </div>


  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
