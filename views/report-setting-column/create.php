<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReportSettingColumn */

?>
<div class="report-setting-column-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
