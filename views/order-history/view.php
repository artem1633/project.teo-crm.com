<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OrderHistory */
?>
<div class="order-history-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'order_id',
            'history_comment:ntext',
            'created_at',
        ],
    ]) ?>

</div>
