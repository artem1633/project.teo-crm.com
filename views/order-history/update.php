<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrderHistory */
?>
<div class="order-history-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
