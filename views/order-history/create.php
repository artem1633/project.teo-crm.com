<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrderHistory */

?>
<div class="order-history-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
