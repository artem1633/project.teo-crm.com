<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerContact */
?>
<div class="customer-contact-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'position',
            'phone',
            'duty_phone',
            'email:email',
            'customer_id',
        ],
    ]) ?>

</div>
