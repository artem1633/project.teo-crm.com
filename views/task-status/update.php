<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaskStatus */
?>
<div class="task-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
