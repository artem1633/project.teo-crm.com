<?php

use app\models\TaskStatusWorkType;
use app\models\WorkType;
use kartik\color\ColorInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaskStatus */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord == false) {
    $model->workTypes = ArrayHelper::getColumn(TaskStatusWorkType::find()->where(['task_status_id' => $model->id])->all(), 'work_type_id');
    $model->workTypes = ArrayHelper::getColumn(WorkType::find()->where(['id' => $model->workTypes])->all(), 'id');
}
?>

<div class="task-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'state')->checkbox([
        'uncheck'=>0,
        'checked'=>1,
    ],true) ?>
    <?= $form->field($model, 'successful')->checkbox([
        'uncheck'=>0,
        'checked'=>1,
    ],true) ?>
    <?= $form->field($model, 'unsuccessful')->checkbox([
        'uncheck'=>0,
        'checked'=>1,
    ],true) ?>

    <?= $form->field($model, 'color')->widget(ColorInput::classname(), [
        'options' => ['placeholder' => 'Выберите цвет ...'],
    ]); ?>


  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
