<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WorkHours */

?>
<div class="work-hours-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
