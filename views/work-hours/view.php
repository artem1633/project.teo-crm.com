<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WorkHours */
?>
<div class="work-hours-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'task_id',
            'hours_count',
            'work_done',
            'comment',
            'author_id',
            'created_at',
        ],
    ]) ?>

</div>
