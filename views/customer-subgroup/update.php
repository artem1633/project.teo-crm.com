<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerSubgroup */
?>
<div class="customer-subgroup-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
