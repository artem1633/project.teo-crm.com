<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerSubgroup */
?>
<div class="customer-subgroup-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'customer_id',
        ],
    ]) ?>

</div>
