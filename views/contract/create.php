<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Contract */

?>
<div class="contract-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
