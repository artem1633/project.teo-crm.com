<?php


use app\models\Contract;
use app\models\OrderStatus;
use app\models\OrderWorkType;

use app\models\WorkType;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{addtask} {view} {update} {delete}',
        'buttons' => [
            'addtask' => function($url, $model){
                return   Html::a('<i class="fa fa-plus"></i>', ['task/create', 'order_id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'],
                    ['role'=>'modal-remote','title'=> 'Добавить Задачу']);
            }
        ],
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to(['order/'.$action,'id'=>$key, 'contract_id' => $model->contract_id, 'containerPjaxReload' => '#pjax-container-info-container']);
        },
//        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],

//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
//        // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    //
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '50px',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detailUrl' => function ($model) {
//            return Url::toRoute(['view', 'id' => $model->id]);
        },
        'detail' => function ($model, $key, $index, $column) {
            $filesSearchModel = new \app\models\TaskSearch();
            $filesDataProvider = $filesSearchModel->search([]);
            $filesDataProvider->query->andWhere(['order_id' => $model->id]);

            return \Yii::$app->controller->renderPartial('_task-table', [
                'searchModel' => $filesSearchModel,
                'dataProvider' => $filesDataProvider,
                'model' => $model,
            ]);
        },
        'headerOptions' => ['class' => 'kartik-sheet-style'],
        'expandOneOnly' => true
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'hours_plane',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'hours_fact',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order_status_id',
        'content' => function($data){
            $status = ArrayHelper::getColumn(OrderStatus::find()->where(['id' => $data->order_status_id])->all(), 'name');
            return implode('',$status);

        },
        'filter' => ArrayHelper::map(OrderStatus::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order_date',
    ],

//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'order_deadline',
//        'filterInputOptions' => ['class' => 'form-control', 'type' => 'date'],
//    ],



    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'expired',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'scan_added',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'related_orders',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
        'width' => '10%',
    ],
//    [
//        'attribute'=>'construction_type',
//        'label' => 'Статус',
//        'filter'=>array(
//            'Модернизация' => 'Модернизация',
//            'Строительство' => 'Строительство',
//        ),
        //
//        'content' => function($model){
//            $icon = '';
//
//            if($model->construction_type == 'Модернизация'){
//                $icon = '<i class="fa fa-wrench" style="font-size: 15px;"></i>';
//            } else if($model->construction_type == 'Строительство') {
//                $icon = '<i class="fa fa-book" style="font-size: 15px;"></i>';
//            }
//
//            return $icon;
//        },
//        'width' => '4%',
//        'hAlign' => GridView::ALIGN_CENTER,
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'ams_type_id',
//        'content' => function($data){
//            $ams = ArrayHelper::getColumn(AmsType::find()->where(['id' => $data->ams_type_id])->all(), 'name');
//            return implode('',$ams);
//
//        },
//        'filter' => ArrayHelper::map(AmsType::find()->asArray()->all(), 'id', 'name'),
//        'filterType' => GridView::FILTER_SELECT2,
//        'filterWidgetOptions' => [
//            'options' => ['prompt' => ''],
//            'pluginOptions' => ['allowClear' => true],
//        ],
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'placement_id',
//        'content' => function($data){
//            $placement = ArrayHelper::getColumn(Placement::find()->where(['id' => $data->placement_id])->all(), 'name');
//            return implode('',$placement);
//
//        },
//        'filter' => ArrayHelper::map(Placement::find()->asArray()->all(), 'id', 'name'),
//        'filterType' => GridView::FILTER_SELECT2,
//        'filterWidgetOptions' => [
//            'options' => ['prompt' => ''],
//            'pluginOptions' => ['allowClear' => true],
//        ],
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'author_id',
//    ],










];