<?php

use app\models\ContractToWorkType;
use app\models\Customer;
use app\models\WorkType;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\JsExpression;

$searchModel = new \app\models\ContractSearch();

return [
//    [
//        'class' => 'kartik\grid\DataColumn',
//        'width' => '30px',
//    ],
//         [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'id',
//     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$model->id]);
        },
//        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'project_name',
        'content' => function($model){
            return Html::a($model->project_name, ['view', 'id' => $model->id], ['data-pjax' => 0]);
        }
    ],
    [
        'attribute'=>'contract_status',
        'label' => 'Статус',
        'filterType' => GridView::FILTER_SELECT2,
        'filter'=>array(
            'в Работе' => '<i class="fa fa-cog" style="font-size: 15px;" title="в Работе"></i>',
            'в Архиве' => '<i class="fa fa-book" style="font-size: 15px;" title="в Архиве"></i>',
        ),
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'placeholder' => '',
                'allowClear' => true,
                'escapeMarkup' => new JsExpression("function(m) { return m; }"),
            ],
        ],
        'content' => function($model){
            $icon = '';

            if($model->contract_status == 'в Работе'){
                $icon = '<i class="fa fa-cog" style="font-size: 15px;" title="в Работе"></i>';
            } else if($model->contract_status == 'в Архиве') {
                $icon = '<i class="fa fa-book" style="font-size: 15px;" title="в Архиве"></i>';
            }

            return $icon;
        },
        'width' => '1%',
        'hAlign' => GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'contract_date',
//        'filterInputOptions' => ['class' => 'form-control', 'type' => 'date'],
        'filterInputOptions' => ['autocomplete' => 'off', 'class' => 'form-control'],
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginEvents' => [
                'cancel.daterangepicker'=>'function(ev, picker) {$("#contractsearch-contract_date").val(""); $("#contractsearch-contract_date").trigger("change"); }'
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'convertFormat'=>true,
                'locale' => [
                    'cancelLabel' => 'Clear',
                    'format' => 'YYYY-MM-DD'
                ]
            ],
        ],
    ],
    [
        'attribute'=>'contract_type',
        'label' => 'Вид',
        'filter'=>array(
            'c Подрядчиком' => '<i class="fa fa-wrench" style="font-size: 15px;" title="с Подрядчиком"></i>\'',
            'с Заказчиком' => '<i class="fa fa-usd" style="font-size: 15px;" title="с Заказчиком"></i>',
            ),
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'placeholder' => '',
                'allowClear' => true,
                'escapeMarkup' => new JsExpression("function(m) { return m; }"),
            ],
        ],
        'filterType' => GridView::FILTER_SELECT2,
        'content' => function($model){
            $icon = '';

            if($model->contract_type == 'c Подрядчиком'){
                $icon = '<i class="fa fa-wrench" style="font-size: 15px;" title="с Подрядчиком"></i>';
            } else if($model->contract_type == 'с Заказчиком') {
                $icon = '<i class="fa fa-usd" style="font-size: 15px;" title="с Заказчиком"></i>';
            }

            return $icon;
        },
        'width' => '1%',
        'hAlign' => GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customer_id',
        'content' => function($data){
            $customer = ArrayHelper::getColumn(Customer::find()->where(['id' => $data->customer_id])->all(), 'name');
            return implode('',$customer);

        },
        'filter' => ArrayHelper::map(Customer::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'city_id',
        'value' => 'city.name',
        'filter' => ArrayHelper::map(\app\models\City::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
        'width' => '10%',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'contract_number',
//        'content' => function($model){
//            return Html::a($model->contract_number, ['view', 'id' => $model->id], ['data-pjax' => 0]);
//        }
//    ],
    [
//        'label' => 'Закрыты заказы',
        'content' => function($data){
            $openOrder = \app\models\Order::find()->where(['contract_id' => $data->id, 'is_closed' => 0])->one();
            return $openOrder ? '<i title="Закрыты открыты" class="fa fa-times text-danger"></i>' : '<i title="Закрыты закрыты" class="fa fa-check text-success"></i>';
        }
    ],

//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'scan_id',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'comment',
//     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'contract_executor_name',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'post_address',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'scan_id',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'author_id',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
    // ],


];   