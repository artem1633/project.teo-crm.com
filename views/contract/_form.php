<?php

use app\models\City;
use app\models\ContractToWorkType;
use app\models\ContractUserGroup;
use app\models\Customer;
use app\models\User;
use app\models\WorkType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;


/* @var $this yii\web\View */
/* @var $model app\models\Contract */
/* @var $form yii\widgets\ActiveForm */


if($model->isNewRecord == false) {
    $model->userGroups = ArrayHelper::getColumn(ContractUserGroup::find()->where(['contract_id' => $model->id])->all(), 'user_id');
    $model->userGroups = ArrayHelper::getColumn(User::find()->where(['id' => $model->userGroups])->all(), 'id');

    if($model->customer_id){
        $customer = Customer::findOne($model->customer_id);
        if($customer){
            $model->customerName = $customer->name;
        }
    }
    if($model->city_id){
        $city = City::findOne($model->city_id);
        if($city){
            $model->cityName = $city->name;
        }
    }
}



?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<div class="contract-form">
    <?= $form->field($model, 'author_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false)?>
    <div class="row">

        <div class="col-md-3">
            <?= $form->field($model, 'project_name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'contract_number')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'contract_date')->input('date') ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'customerName')->widget(\kartik\select2\Select2::class, [
                'data' => ArrayHelper::map(Customer::find()->orderBy('name asc')->all(), 'name', 'name'),
                'options' => ['placeholder' => 'Контрагент'],
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [','],
                ],
            ]) ?>
        </div>



        <div class="col-md-3">
            <?= $form->field($model, 'contract_status')->dropDownList([
                'в Работе' => 'в Работе',
                'в Архиве' => 'в Архиве',
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cityName')->widget(\kartik\select2\Select2::class, [
                'data' => ArrayHelper::map(City::find()->orderBy('name asc')->all(), 'name', 'name'),
                'options' => ['placeholder' => 'Город'],
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [','],
                ],
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'userGroups')->widget(\kartik\select2\Select2::className(), [
                'data' => ArrayHelper::map(User::find()->all(), 'id', 'name'),
                'options' => [
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'tags' => false,
                    'tokenSeparators' => [','],
                ],
            ]) ?>
        </div>






    </div>
        <hr class="hor-line">
    <div class="row">
     <div class="col-md-3">
            <?= $form->field($model, 'contract_deadline')->input('date') ?>
        </div>




     <div class="col-md-3">
            <?= $form->field($model, 'post_address')->textInput(['maxlength' => true])->label('Почтовый адрес - Индекс, город, адрес') ?>
    </div>

    <div class="col-md-3">
        <?= $form->field($model, 'contract_type')->dropDownList([
            'с Заказчиком' => 'с Заказчиком',
            'c Подрядчиком' => 'c Подрядчиком',
        ]) ?>
    </div>

     <div class="row">
         <div class="col-md-12">
             <?= $form->field($model, 'listFile')->widget(MultipleInput::className(), [

                 'id' => 'my_id',
                 'min' => 0,
                 'addButtonOptions' => [
                     'label' => 'Добавить ещё документ',
                 ],
                 'columns' => [
                     [
                         'name' => 'id',
                         'options' => [
                             'type' => 'hidden'
                         ]
                     ],
                     [
                         'name' => 'name',
                         'title' => 'Название',
                     ],
                     [
                         'name' => 'file_new',
                         'title' => 'Файл',
                         'type'  => 'fileInput',
                         'options' => [
                             'pluginOptions' => [
                                 'initialPreview'=>[
                                     //add url here from current attribute
                                 ],
                                 'showPreview' => false,
                                 'showCaption' => true,
                                 'showRemove' => true,
                                 'showUpload' => false,
                             ]
                         ]
                     ],

                 ],
             ])->label(false) ?>
         </div>
     </div>


    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
        </div>

    </div>







    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

         </div>


<?php ActiveForm::end(); ?>


