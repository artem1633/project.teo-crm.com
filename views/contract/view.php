<?php

use app\models\TaskSearch;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Contract */
/* @var $chatModel app\models\Chat */
/* @var $message app\models\Chat */
$contract_id = $model->id;
?>
<?php Pjax::begin(['id' => 'pjax-container-info-container', 'enablePushState' => false]); ?>
<div class="contract-view" style="margin-top: -60px">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Заказы</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                        <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['order/create', 'contract_id' => $contract_id, 'containerPjaxReload' => '#pjax-container-info-container'],
                            ['role'=>'modal-remote','title'=> 'Добавить спринт','class'=>'btn btn-warning btn-xs'])?>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <?= $this->render('@app/views/contract/order-index', [
                            'searchModel' => $orderSearchModel,
                            'dataProvider' => $orderDataProvider,
                            'contract_id' => $model->id,
                        ]) ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Календарь Задач</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>


                <div class="panel-body" style="height: 350px">
                    <div class="col-md-12">
                        <?php $this->beginContent('@app/views/contract/status.php',[
                            'model' => $model,
                        ]); ?>

                        <?php $this->endContent(); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">

        <div class="col-md-5">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Информация</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;">
                        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['contract/update', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'], ['role' => 'modal-remote', 'class' => 'btn btn-warning btn-xs']) ?>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="height: 450px; overflow-y: auto;">
                    <div class="row">
                       <div class="col-md-12">
                           <div class="col-md-12 col-sm-12 col-xs-12">
                               <?= DetailView::widget([
                                   'model' => $model,
                                   'attributes' => [
                                       [
                                           'attribute' => 'customer_id',
                                           'value' => ArrayHelper::getValue($model->getCustomer()->one(),'name'),
                                           'visible' => Yii::$app->user->identity->can('contract_view_customer'),
                                       ],
                                       'contract_number',
                                       'contract_deadline',
                                       'post_address',
                                       [
                                           'attribute' => 'city_id',
                                           'value' => ArrayHelper::getValue($model->getCity()->one(),'name')
                                       ],
                                       'project_name',
                                       'contract_date',
                                       [
                                           'attribute' => 'author_id',
                                           'value' => ArrayHelper::getValue($model->getAuthor()->one(),'name')
                                       ],
//                                    'created_at',

                                   ],
                               ]) ?>
                           </div>
                       </div>
                    </div>
                </div>
            </div>





        </div>

        <div class="col-md-7">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Документы</h4>
                        <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                            <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['scan/create', 'contract_id' => $contract_id, 'containerPjaxReload' => '#pjax-container-info-container'],
                                ['role'=>'modal-remote','title'=> 'Добавить документ','class'=>'btn btn-warning btn-xs'])?>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>


                    <div class="panel-body " style="height: 500px; overflow-y: auto;height: 200px"">
                    <div class="col-md-12">
                        <?= $this->render('@app/views/contract/scan_index', [
                            'searchModel' => $scanSearchModel,
                            'dataProvider' => $scanDataProvider,
                            'contract_id' => $model->id,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
            <div class="col-md-12">
                <div id='work_chat_mes' class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Чат</h4>
                        <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>
                    <div class="panel-body" id="messageList" style="height: 500px; overflow-y: auto;height: 200px;scroll;">
                        <?php echo \rmrevin\yii\module\Comments\widgets\CommentListWidget::widget(['entity' => (string) $model->id,]);?>
                    </div>

                </div>
            </div>
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Платежи</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">

                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>




                <div class="panel-body"  style="height: 500px; overflow-y: auto;height: 200px;display: none">
                    <?= $this->render('@app/views/payments/index', [
                        'searchModel' => $paySearchModel,
                        'dataProvider' => $payDataProvider,

                    ]) ?>

                </div>
            </div>

        </div>
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">История изменений</h4>
                        <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>
                    <div class="panel-body"  style="height: 500px; overflow-y: auto;display: none"">
                    <?= $this->render('@app/views/history/index', [
                        'searchModel' => $historySearchModel,
                        'dataProvider' => $historyDataProvider,
                        'contract_id' => $model->id,
                    ]) ?>

                </div>
            </div>

        </div>





</div>



    <script type="text/javascript">
        var div = $("#messageList");
        div.scrollTop(div.prop('scrollHeight'));

    </script>
    <?php

    $this->registerJs(
        '$("document").ready(function(){
         $("#form-pjax-container").on("pjax:end", function() {
            $.pjax.reload({container:"#chat-pjax-container"});  //Reload GridView
            return
         });
    });'
    );

    ?>
        </div>

    </div>

<?php Pjax::end() ?>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'fade modal-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>



