<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $message = new \app\models\Chat(['contract_id' => $model->id]);
$form = ActiveForm::begin(['options' => ['data-pjax' => true], 'id' => 'chat-form', 'action' => ['contract/send-message']]) ?>
<?= $form->field($message, 'text')->textarea()->label(false) ?>
<div class="hidden">
    <?= $form->field($message, 'contract_id')->hiddenInput()->label(false); ?>
</div>
<?= Html::submitButton('Отправить', ['id' => 'submit-button-chat-form', 'class' => 'btn btn-primary btn-block']); ?>
<?php ActiveForm::end() ?>

<?php

$script = <<< JS

$('#submit-button-chat-form').click(function(e){
    e.preventDefault();
    
    $.ajax({
        method: 'POST',
        url: $('#chat-form').attr('action'),
        data: $('#chat-form').serialize(),
        success: function(response){
            $.pjax.reload('#form-pjax-container');
        }
    });
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>