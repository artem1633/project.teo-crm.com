<?php

use kartik\grid\GridView;
use yii\helpers\Html;

?>

<?=GridView::widget([
    'id'=>'crud-datatable-'.$model->id,
    'dataProvider' => $dataProvider,
    'pjax'=>true,
    'columns' => require(__DIR__.'/_task-columns.php'),
    'panelBeforeTemplate' => '',
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
])?>