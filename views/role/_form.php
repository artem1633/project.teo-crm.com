<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Role */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="role-form">

    <?php $form = ActiveForm::begin(); ?>


        <div class="row">

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'contract_create')->checkbox([
                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'contract_update')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'contract_delete')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'contract_view')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true)?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'contract_view_all')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'contract_view_sum')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'contract_view_customer')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'order_create')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true)?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'order_update')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'order_delete')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'order_view')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'order_view_all')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'order_view_sum')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'task_create')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'task_update')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'task_delete')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'task_view')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'task_view_all')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'task_sum')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'task_time')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'task_payment')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'task_responsible')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'user_create')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'user_update')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'user_delete')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'user_view')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'user_view_all')->checkbox([

                            'uncheck'=>0,
                            'checked'=>1,
                        ],true) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <?= $form->field($model, 'directory_access')->checkbox([

                        'uncheck'=>0,
                        'checked'=>1,
                    ],true) ?>
                </div>
                <div class="row">
                    <?= $form->field($model, 'report_access')->checkbox([
                            'uncheck'=>0,
                            'checked'=>1,
                    ],true) ?>
                </div>
            </div>
        </div>

    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
</div>
