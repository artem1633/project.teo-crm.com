<?php

use yii\helpers\Url;

?>

<div id="sidebar" class="sidebar">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\Menu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
//                    ['label' => 'Вакансии', 'icon' => 'fa  fa-th', 'url' => ['/project'],],
//                    ['label' => 'Кандидаты', 'icon' => 'fa  fa-users', 'url' => ['/candidate'],],

                    ['label' => 'Проекты', 'icon' => 'fa fa-briefcase', 'url' => ['/contract'],],
                    ['label' => 'Спринты', 'icon' => 'fa  fa-flag', 'url' => ['/order'],],
                    ['label' => 'Задачи', 'icon' => 'fa fa-sitemap', 'url' => ['/task'],],
                    ['label' => 'Очет', 'icon' => 'fa fa-area-chart', 'url' => ['/report'],],

//
                    ['label' => 'Справочники', 'icon' => 'fa fa-book', 'url' => '/user', 'options' => ['class' => 'has-sub'],'visible' =>Yii::$app->user->identity->can('directory_access'),
                        'items' => [
                            ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                            ['label' => 'Контрагенты', 'icon' => 'fa fa-exchange', 'url' => ['/customer'],],
                            ['label' => 'Роли', 'icon' => 'fa fa-users', 'url' => ['/role'],],
                            ['label' => 'Ключевики',  'icon' => 'fa fa-cog','url' => ['/work-type'],],
                            ['label' => 'Города', 'icon' => 'fa fa-map-marker','url' => ['/city'],],
                            ['label' => 'Статусы заказов','icon' => 'fa fa-bookmark-o','url' => ['/order-status'],],
                            ['label' => 'Статусы задач', 'icon' => 'fa fa-tag', 'url' => ['/task-status'],],
                            ['label' => 'Настройки колонок', 'icon' => 'fa fa-sliders','url' => ['/report-setting-column'],],
//
                    ]],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
