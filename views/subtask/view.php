<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Subtask */
?>
<div class="subtask-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'desc:ntext',
            'task_id',
            'status',
        ],
    ]) ?>

</div>
