<?php


use yii\helpers\Url;

/** @var \app\models\User $identity */
$identity = Yii::$app->user->identity;
return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
        'visible' => $full,

    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'desc',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'filter' => [0 => 'Нет', 1 => 'Да'],
        'attribute' => 'done',
        'content' => function (\app\models\Subtask $model) {
            if ($model->status) {
                return 'Да';
            }
            return 'Нет';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'task_id',
        'content' => function (\app\models\Subtask $model) {
            return $model->task ? $model->task->name : '';
        },
        'visible' => $full,
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => '{update} {delete}',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'
        ],
    ],

];




























//use yii\helpers\Url;
//
//return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
//        // [
//        // 'class'=>'\kartik\grid\DataColumn',
//        // 'attribute'=>'id',
//    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'desc',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'task_id',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'status',
//    ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//                return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
//        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
//                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                          'data-request-method'=>'post',
//                          'data-toggle'=>'tooltip',
//                          'data-confirm-title'=>'Are you sure?',
//                          'data-confirm-message'=>'Are you sure want to delete this item'],
//    ],
//
//];

