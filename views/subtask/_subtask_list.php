<?php

use app\models\User;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

/** @var User $identity */
$identity = Yii::$app->user->identity;
?>

<div class="subtask-list">
    <?php /** @var \app\models\Task $model */
    foreach ($dataProvider->getModels() as $model): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="item-task">
                    <div class="item-task-text">
                        <p> <?php if ($model->status): ?>
                        <p> <i class="fa fa-check" style="color: green;"></i>&nbsp;Выполнено</p>
                        <?php endif; ?>
                        <p><?= $model->desc ?></p>
                    </div>
                    <!--Кнопки-->
                    <div class="item-task-btn">
                        <?= Html::a('<i class="fa fa-check"></i>',
                            ['/subtask/status', 'id' => $model->id], [
                                'role' => 'modal-remote',
                                'title' => 'Завершить',
                                'data-toggle' => 'tooltip',
                                'style' => 'color: green;',
                                'hidden' => (bool)$model->status,
                            ]) ?>
                        &nbsp;
                        <?= Html::a('<i class="fa fa-pencil"></i>',
                            ['/subtask/update', 'id' => $model->id], [
                                'role' => 'modal-remote',
                                'title' => 'Редактирование',
                                'data-toggle' => 'tooltip',
                                'hidden' => (bool)$model->status,
                            ]) ?>
                        &nbsp;
                        <?= Html::a('<i class="fa fa-trash"></i>',
                            ['/subtask/delete', 'id' => $model->id], [
                                'style' => 'color: red;',
                                'role' => 'modal-remote',
                                'title' => 'Удаление',
                                'data-toggle' => 'tooltip',
                                'data-confirm' => false,
                                'data-method' => false,// for overide yii data api
                                'data-request-method' => 'post',
                                'data-confirm-title' => 'Удаление задачи',
                                'data-confirm-message' => 'Подвердите удаление задачи',
                                'data-confirm-ok' => 'Удалить',
                                'data-confirm-cancel' => 'Отмена',
                            ]) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>