<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubtaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



CrudAsset::register($this);

?>
<div class="subtask-index">
    <?php $form = ActiveForm::begin([
        'action' => '/subtask/create',
    ]) ?>
    <div class="row">

        <div class="col-md-10">
            <?= $form->field($model, 'desc')->textInput([
                'placeholder' => 'Введите текст подзадачи и нажмите ОК'
            ])->label(false) ?>
            <?= $form->field($model, 'status')->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'task_id')->hiddenInput(['value' => $task_id])->label(false); ?>
        </div>
        <div class="col-md-2">
            <?= Html::submitButton('Ок', [
                'class' => 'btn btn-info btn-block',
//                'role' => 'modal-remote'
            ]) ?>
        </div>
        <?= $form->field($model, 'status')->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'task_id')->hiddenInput(['value' => $task_id])->label(false); ?>
    </div>
    <?php ActiveForm::end() ?>
    <div class="row">
        <div class="col-md-12">
            <!--            --><?php //Pjax::begin([
            //                'id' => 'task-list-pjax',
            //                'enablePushState' => false
            //            ]) ?>
            <?= $this->render('_subtask_list', [
                'dataProvider' => $dataProvider
            ]) ?>
            <!--            --><?php //Pjax::end() ?>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
