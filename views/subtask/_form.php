<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Subtask */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subtask-form">


        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'task_id')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'desc')->textInput(['maxlength' => true]) ?>





        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>


    </div>
