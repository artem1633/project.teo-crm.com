<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subtask */
?>
<div class="subtask-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

