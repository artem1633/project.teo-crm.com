<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use app\widgets\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ChatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chats';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

    <div class="panel-body" style="height: 60vh; overflow-y: scroll;">
        <?php \yii\widgets\Pjax::begin(['id' => 'chat-pjax-container', 'enablePushState' => false]) ?>

<!--

    </div>
    <div class="panel-footer">
        <?php \yii\widgets\Pjax::begin(['id' => 'form-pjax-container', 'enablePushState' => false]) ?>
        <?= $this->render('_form',[
            'model' => $model,
        ]) ?>
        <?php \yii\widgets\Pjax::end() ?>
    </div>


