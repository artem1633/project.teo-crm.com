<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $message = new \app\models\Chat(['contract_id' => $model->id]);
$form = ActiveForm::begin(['options' => ['data-pjax' => true], 'id' => 'chat-form', 'action' => ['ticket/send-message']]) ?>
<?= $form->field($message, 'text')->textarea()->label(false) ?>
    <div class="hidden">
        <?= $form->field($message, 'contract_id')->hiddenInput()->label(false); ?>
    </div>

<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary btn-block']); ?>
<?php ActiveForm::end() ?>