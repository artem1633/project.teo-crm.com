<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Chat */
?>
<div class="chat-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'contract_id',
            'author_id',
            'text:ntext',
            'created_at',
        ],
    ]) ?>

</div>
