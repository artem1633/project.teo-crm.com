<?php


use app\models\Contract;
use app\models\OrderStatus;
use app\models\OrderWorkType;

use app\models\WorkType;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$model->id]);
        },
//        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
//        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    //


    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'hours_fact',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'hours_plane',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order_date',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'contract_id',
        'content' => function($data){
            $contract = ArrayHelper::getColumn(Contract::find()->where(['id' => $data->contract_id])->all(), 'project_name');
            return implode('',$contract);

        },
        'filter' => ArrayHelper::map(Contract::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order_deadline',
        'filterInputOptions' => ['class' => 'form-control', 'type' => 'date'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order_status_id',
        'content' => function($data){
            $status = ArrayHelper::getColumn(OrderStatus::find()->where(['id' => $data->order_status_id])->all(), 'name');
            return implode('',$status);

        },
        'filter' => ArrayHelper::map(OrderStatus::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Просрочен',
        'content' => function($model){
            if($model->isExpired()){
                return '<i class="fa fa-check text-success"></i>';
            } else {
                return '<i class="fa fa-times text-danger"></i>';
            }
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'expired',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'scan_added',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'related_orders',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
        'width' => '10%',
    ],

//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'author_id',
//    ],











];   