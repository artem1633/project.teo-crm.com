<?php

use app\models\OrderWorkType;
use app\models\WorkType;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
$order_id = $model->id;
?>
<?php Pjax::begin(['id' => 'pjax-container-info-container', 'enablePushState' => false]); ?>
<div class="order-view" style="margin-top:-60px ">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Задачи</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                        <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['task/create', 'order_id' => $order_id, 'containerPjaxReload' => '#pjax-container-info-container'],
                            ['role'=>'modal-remote','title'=> 'Добавить задачу','class'=>'btn btn-warning btn-xs'])?>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body"  style="height: 350px; overflow-y: auto;"">
                <?= $this->render('@app/views/order/task_index', [
                    'searchModel' => $taskSearchModel,
                    'dataProvider' => $taskDataProvider,
                    'order_id' => $model->id,
                ]) ?>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Календарь Задач</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>


                <div class="panel-body" style="height: 350px">
                    <div class="col-md-12">
                        <?php $this->beginContent('@app/views/order/status.php',[
                            'model' => $model,
                        ]); ?>

                        <?php $this->endContent(); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
<div class="row">

    <div class="col-md-5">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Информация</h4>
                <div class="panel-heading-btn" style="margin-top:-20px;">
                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['order/update', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'], ['role' => 'modal-remote', 'class' => 'btn btn-warning btn-xs']) ?>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
            </div>
            <div class="panel-body"  style="height: 450px; overflow-y: auto;">
                <div class="row">
                    <div class="col-md-12">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                    'name',
                                [
                                    'attribute' => 'contract_id',
                                    'value' => ArrayHelper::getValue($model->getContract()->one(),'project_name')
                                ],

                                'order_sum',
                                [
                                        'attribute' => 'author_id',
                                        'value' => ArrayHelper::getValue($model->getAuthor()->one(),'name')
                                ],
                                [
                                    'attribute' => 'order_status_id',
                                    'value' => ArrayHelper::getValue($model->getOrderStatus()->one(),'name')
                                ],
                                'order_date',
                                'order_deadline',
                                'fact_end_date',
                                'order_deadline',
                                'order_sum',
                                'hours_plane',
                                'hours_fact',
                                'comment',
                                'created_at',
                                'updated_at'
                            ],
                        ]) ?>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <div class="col-md-7">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Документы</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                        <?= Html::a('<span class="glyphicon glyphicon-plus btn-circle"></span>', ['scan/create', 'order_id' => $order_id, 'containerPjaxReload' => '#pjax-container-info-container'],
                            ['role'=>'modal-remote','title'=> 'Добавить документ','class'=>'btn btn-warning btn-xs'])?>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>


                <div class="panel-body" style="height: 500px; overflow-y: auto;height: 200px">
                    <div class="col-md-12">
                        <?= $this->render('@app/views/order/scan_index', [
                            'searchModel' => $scanSearchModel,
                            'dataProvider' => $scanDataProvider,
                            'order_id' => $model->id,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div id='work_chat_mes' class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Чат</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" id="messageList" style="height: 500px; overflow-y: auto;height: 200px;scroll;">
                    <?php echo \rmrevin\yii\module\Comments\widgets\CommentListWidget::widget(['entity' => (string) 'orders-'.$model->id,]);?>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">История изменений</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body"  style="height: 500px; overflow-y: auto;display: none"">
                <?= $this->render('@app/views/order-history/index', [
                    'searchModel' => $orderHistorySearchModel,
                    'dataProvider' => $orderHistoryDataProvider,
                    'order_id' => $model->id,
                ]) ?>

            </div>
        </div>
    </div>
    </div>
</div>

</div>










    <?php Pjax::end() ?>
    <?php Modal::begin([
        "id"=>"ajaxCrudModal",
        'options' => ['class' => 'fade modal-slg'],
        "footer"=>"",// always need it for jquery plugin
    ])?>
    <?php Modal::end(); ?>
