<?php


use app\models\Contract;
use app\models\OrderStatus;


use app\models\User;
use app\models\WorkType;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */


$contractHidden = false;

if($contract_id != null){
    $contractHidden = true;
}

if($model->isNewRecord == false){
    $model->userGroup = ArrayHelper::getColumn(\app\models\OrderUserGroup::find()->where(['order_id' => $model->id])->all(), 'user_id');
    $model->userGroup = ArrayHelper::getColumn(User::find()->where(['id' => $model->userGroup])->all(), 'id');
}

?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="order-form">
    <?= $form->field($model, 'author_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false)?>

    <div class="row">
        <div class="col-md-4<?=($contractHidden ? ' hidden' : '')?>">
            <?= $form->field($model, 'contract_id')->dropDownList(ArrayHelper::map(Contract::find()->all(), 'id', 'project_name')) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'hours_plane')->textInput(['maxlength' => true]) ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'order_date')->input('date') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'order_deadline')->input('date') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'userGroup')->widget(\kartik\select2\Select2::class, [
                'data' => ArrayHelper::map(User::find()->all(), 'id', 'name'),
                'options' => [
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'tags' => false,
                    'tokenSeparators' => [','],
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
        </div>
    </div>
    <div class="row">
        <?php if(Yii::$app->user->identity->can('order_view_sum')): ?>
            <div class="col-md-4">
                <?= $form->field($model, 'order_sum')->textInput(['maxlength' => true]) ?>
            </div>
        <?php endif; ?>
        <div class="col-md-4">
            <?= $form->field($model, 'order_status_id')->dropDownList(ArrayHelper::map(OrderStatus::find()->orderBy('name asc')->all(), 'id', 'name')) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'is_closed')->checkbox() ?>
        </div>
    </div>


    <div class="row">
    </div>


    <div class="row">

    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'listFile')->widget(MultipleInput::className(), [

                'id' => 'my_id',
                'min' => 0,
                'columns' => [
                    [
                        'name' => 'id',
                        'options' => [
                            'type' => 'hidden'
                        ]
                    ],
                    [
                        'name' => 'name',
                        'title' => 'Название',
                    ],
                    [
                        'name' => 'file_new',
                        'title' => 'Файл',
                        'type'  => 'fileInput',
                        'options' => [
                            'pluginOptions' => [
                                'initialPreview'=>[
                                    //add url here from current attribute
                                ],
                                'showPreview' => false,
                                'showCaption' => true,
                                'showRemove' => true,
                                'showUpload' => false,
                            ]
                        ]
                    ],

                ],
            ])->label(false) ?>
        </div>
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
