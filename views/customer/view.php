<?php

use app\models\CustomerContact;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use app\models\CustomerSubgroup;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
?>
<div class="customer-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'address',
            [
                'attribute' => 'id',
                'label' => 'Подгруппы',
                'value' => function($model){
                    $data = ArrayHelper::getColumn(CustomerSubgroup::find()->where(['customer_id' => $model->id])->all(), 'name');
                    return implode(', ', $data);
                },
            ],
            [
                'attribute' => 'id',
                'label' => 'Контакты',
                'value' => function($model){
                    $data = CustomerContact::find()->where(['customer_id' => $model->id])->all();
                    $output = '';

                    foreach ($data as $contact)
                    {
                        $str = implode(' ', [$contact->name, $contact->position, $contact->phone, $contact->duty_phone, $contact->email]);
                        $output .= "{$str}<br>";
                    }


                    return $output;
                },
                'format' => 'html',
            ],
        ],
    ]) ?>

</div>
