<?php

use app\models\CustomerContact;
use app\models\CustomerSubgroup;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'subgroups')->widget(\kartik\select2\Select2::className(), [
        'data' => ArrayHelper::map(CustomerSubgroup::find()->all(), 'name', 'name'),
        'options' => [
            'multiple' => true,
        ],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [','],
        ],
    ])?>

    <?= $form->field($model, 'contacts')->widget(MultipleInput::className(), [

            'id' => 'my_id',
            'min' => 0,
            'columns' => [
            [
                'name' => 'id',
                'options' => [
                    'type' => 'hidden'
                ]
            ],
            [
                'name' => 'name',
                'enableError' => true,
                'title' => 'ФИО',
            ],
            [
                'name' => 'position',
                'title' => 'Должность',
            ],
            [
                'name' => 'phone',
                'title' => 'Телефон',
            ],
            [
                'name' => 'duty_phone',
                'title' => 'Служебный телефон',
            ],
            [
                'name' => 'email',
                'title' => 'email',
            ],

        ],
    ]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
