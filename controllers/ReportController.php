<?php

namespace app\controllers;

use app\models\forms\ReportSearch;
use app\models\ReportSettingColumn;
use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;

/**
 * Class ReportController
 * @package app\controllers
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new ReportSearch();
        $dataProvider = $model->search(Yii::$app->request->queryParams);
        $columns = [];

//        VarDumper::dump($model->setting, 10,true);
//        exit;

        if($model->setting != null){
            $setting = ReportSettingColumn::findOne($model->setting);
            $columns = $setting->getColumns();
//            VarDumper::dump($setting->getColumns(), 10,true);
//            exit;
        }

        return $this->render('index', [
            'searchModel' => $model,
            'dataProvider' => $dataProvider,
            'columns' => $columns,
        ]);
    }
}