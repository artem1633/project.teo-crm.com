<?php

namespace app\controllers;

use app\behaviors\RoleBehavior;
use app\models\Chat;
use app\models\OrderHistorySearch;
use app\models\ScanSearch;
use app\models\TaskSearch;
use Yii;
use app\models\Order;
use app\models\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'role' => [
                'class' => RoleBehavior::class,
                'actions' => [
                    'create' => 'order_create',
                    'update' => 'order_update',
                    'view' => 'order_view',
                    'delete' => 'order_delete',
//                    'index' => 'order_view_all',
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;

        $orderHistorySearchModel = new OrderHistorySearch();
        $orderHistoryDataProvider = $orderHistorySearchModel->search([]);
        $orderHistoryDataProvider->query->andWhere(['order_id' => $id]);

        $taskSearchModel = new TaskSearch();
        $taskDataProvider = $taskSearchModel->search([]);
        $taskDataProvider->query->andWhere(['order_id' => $id]);

        $scanSearchModel = new ScanSearch();
        $scanDataProvider = $scanSearchModel->search([]);
        $scanDataProvider->query->andWhere(['order_id' => $id]);

//        $chatModel = Chat::find()->where(['order_id' => $id])->all();

//        if($request->isAjax){
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            return [
//                    'title'=> "Заказ #".$id,
//                    'content'=>$this->renderAjax('view', [
//                        'model' => $this->findModel($id),
//                    ]),
//                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
//                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
//                ];
//        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
                'orderHistorySearchModel' => $orderHistorySearchModel,
                'orderHistoryDataProvider' => $orderHistoryDataProvider,
                'taskSearchModel' => $taskSearchModel,
                'taskDataProvider' => $taskDataProvider,
                'scanSearchModel' => $scanSearchModel,
                'scanDataProvider' => $scanDataProvider,
//                'chatModel' => $chatModel,
            ]);
//        }
    }

    public function actionViewStatus($id)
    {
        $request = Yii::$app->request;

        return $this->render('status', [
            'model' => $this->findModel($id),
        ]);

    }

    /**
     * Creates a new Order model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($contract_id = null,$containerPjaxReload = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = new Order();
        $model->contract_id = $contract_id;
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить Спринт",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'contract_id' => $contract_id,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>$containerPjaxReload,
                    'title'=> "Добавление Спринта",
                    'content'=>'<span class="text-success">Добавлено успешно</span>',
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Добавить еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Добавить Спринт",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'contract_id' => $contract_id,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'contract_id' => $contract_id,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $contract_id = null, $containerPjaxReload = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить Спринт #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'contract_id' => $contract_id,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Изменить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>$containerPjaxReload,
                    'forceClose' => true,
                ];
            }else{
                 return [
                    'title'=> "Изменить Спринт #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'contract_id' => $contract_id,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Изменить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'contract_id' => $contract_id,
                ]);
            }
        }
    }

    /**
     * Delete an existing Role model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $containerPjaxReload = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->delete()){
                return ['forceClose'=>true,'forceReload'=>$containerPjaxReload];
            } else {
                return [
                    'title'=> "Ошибка удаления",
                    'content'=>"<span class='text-danger'>Этот Спринт используется</span>",
                    'footer'=> Html::button('ОК',['class'=>'btn btn-block btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing Role model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        $errorCount = 0;
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            if($model->delete() == false){
                $errorCount++;
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($errorCount > 0){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Удаление",
                    'content'=>"<span class='text-warning'>Выбранные записи были удалены, кроме {$errorCount} шт. так как они используются в других записях</span>",
                    'footer'=> Html::button('ОК',['class'=>'btn btn-block btn-default pull-left','data-dismiss'=>"modal"])
                ];
            } else {
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}
