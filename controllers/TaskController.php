<?php

namespace app\controllers;

use app\behaviors\RoleBehavior;
use app\models\PaymentsSearch;
use app\models\ScanSearch;
use app\models\Subtask;
use app\models\SubtaskSearch;
use app\models\WorkHours;
use app\models\WorkHoursSearch;
use Yii;
use app\models\Task;
use app\models\TaskHistorySearch;
use app\models\TaskSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'role' => [
                'class' => RoleBehavior::class,
                'actions' => [
                    'create' => 'task_create',
                    'update' => 'task_update',
                    'view' => 'task_view',
                    'delete' => 'task_delete',
//                    'index' => 'task_view_all',
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $taskHistorySearchModel = new TaskHistorySearch();
        $taskHistoryDataProvider = $taskHistorySearchModel->search([]);
        $taskHistoryDataProvider->query->andWhere(['task_id' => $id]);

        $scanSearchModel = new ScanSearch();
        $scanDataProvider = $scanSearchModel->search([]);
        $scanDataProvider->query->andWhere(['task_id' => $id]);

        $paymentsSearchModel = new PaymentsSearch();
        $paymentsDataProvider = $paymentsSearchModel->search([]);
        $paymentsDataProvider->query->andWhere(['task_id' => $id]);

        $workHoursSearchModel = new WorkHoursSearch();
        $workHoursDataProvider = $workHoursSearchModel->search([]);
        $workHoursDataProvider->query->andWhere(['task_id' => $id]);

        $subtask_model = new Subtask();
        $subtask_model->task_id = $model->id;
        $subtask_model->status = 0;

        $subtaskSearchModel = new SubtaskSearch();
        $subtaskDataProvider = $subtaskSearchModel->search(['task_id' => $model->id]);
        $subtaskDataProvider->query->orderBy(['status' => SORT_ASC]);
        $subtaskDataProvider->query->andWhere(['task_id' => $model->id]);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Задача #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
                'taskHistorySearchModel' => $taskHistorySearchModel,
                'taskHistoryDataProvider' => $taskHistoryDataProvider,
                'scanSearchModel' => $scanSearchModel,
                'scanDataProvider' => $scanDataProvider,
                'paymentsSearchModel' => $paymentsSearchModel,
                'paymentsDataProvider' => $paymentsDataProvider,
                'workHoursSearchModel' => $workHoursSearchModel,
                'workHoursDataProvider' => $workHoursDataProvider,
                'subtaskSearchModel' => $subtaskSearchModel,
                'subtaskDataProvider' => $subtaskDataProvider,
                'subtask_model' => $subtask_model,
            ]);
        }
    }

    /**
     * Creates a new Task model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($order_id = null,$containerPjaxReload = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = new Task();
        $model->order_id = $order_id;
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить задачу",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'order_id' => $order_id,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>$containerPjaxReload,
                    'title'=> "Добавление задачи",
                    'content'=>'<span class="text-success">Добавлено успешно</span>',
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Добавить еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Добавить задачу",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'order_id' => $order_id,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'order_id' => $order_id,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Task model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $order_id = null, $containerPjaxReload = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить задачу #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'order_id' => $order_id,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Изменить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>$containerPjaxReload,
                    'forceClose' => true,
                ];
            }else{
                 return [
                    'title'=> "Изменить задачу #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'order_id' => $order_id,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Изменить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'order_id' => $order_id,
                ]);
            }
        }
    }

    /**
     * Delete an existing Role model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $containerPjaxReload = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->delete()){
                return ['forceClose'=>true,'forceReload'=>$containerPjaxReload];
            } else {
                return [
                    'title'=> "Ошибка удаления",
                    'content'=>"<span class='text-danger'>Эта задача используется</span>",
                    'footer'=> Html::button('ОК',['class'=>'btn btn-block btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing Role model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        $errorCount = 0;
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            if($model->delete() == false){
                $errorCount++;
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($errorCount > 0){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Удаление",
                    'content'=>"<span class='text-warning'>Выбранные записи были удалены, кроме {$errorCount} шт. так как они используются в других записях</span>",
                    'footer'=> Html::button('ОК',['class'=>'btn btn-block btn-default pull-left','data-dismiss'=>"modal"])
                ];
            } else {
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}
