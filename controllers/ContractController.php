<?php

namespace app\controllers;

use app\behaviors\RoleBehavior;
use app\models\AgreementSearch;
use app\models\Chat;
use app\models\ChatSearch;
use app\models\HistorySearch;
use app\models\OrderSearch;
use app\models\PaymentsSearch;
use app\models\Scan;
use app\models\ScanSearch;
use Yii;
use app\models\Contract;
use app\models\ContractSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ContractController implements the CRUD actions for Contract model.
 */
class ContractController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'role' => [
                'class' => RoleBehavior::class,
                'actions' => [
                    'create' => 'contract_create',
                    'update' => 'contract_update',
                    'view' => 'contract_view',
                    'delete' => 'contract_delete',
//                    'index' => 'contract_view_all',
                ],
            ],
        ];
    }

    /**
     * Lists all Contract models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ContractSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionViewStatus($id)
    {
        $request = Yii::$app->request;

        return $this->render('status', [
            'model' => $this->findModel($id),
        ]);

    }


    /**
     * Displays a single Contract model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $historySearchModel = new HistorySearch();
        $historyDataProvider = $historySearchModel->search([]);
        $historyDataProvider->query->andWhere(['contract_id' => $id]);

        $scanSearchModel = new ScanSearch();
        $scanDataProvider = $scanSearchModel->search([]);
        $scanDataProvider->query->andWhere(['contract_id' => $id]);


        $orderSearchModel = new OrderSearch();
        $orderDataProvider = $orderSearchModel->search([]);
        $orderDataProvider->query->andWhere(['contract_id' => $id]);

        $chatModel = Chat::find()->where(['contract_id' => $id])->all();

        $query = new \yii\db\Query();
        $query->select('task.id')->from('task')
            ->leftJoin('`order`','`task`.`order_id`=`order`.`id`')
            ->andWhere(['contract_id' => $id]);
        $command = $query->createCommand();
        $tasks = $command->queryAll();


        foreach ($tasks as $task => $value ){
           $taskId = $value['id'];
            $paySearch = new PaymentsSearch();
            $payDataProvider = $paySearch->search([]);
            $payDataProvider->query->andWhere(['task_id' => $taskId]);
        }





//        if($request->isAjax){
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            return [
//                    'title'=> "Информация",
//                    'content'=>$this->renderAjax('view', [
//                        'model' => $this->findModel($id),
//                    ]),
//                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
//                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
//                ];
//        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
                'historySearchModel' => $historySearchModel,
                'historyDataProvider' => $historyDataProvider,
                'scanSearchModel' => $scanSearchModel,
                'scanDataProvider' => $scanDataProvider,
                'orderSearchModel' => $orderSearchModel,
                'orderDataProvider' => $orderDataProvider,
                'chatModel' => $chatModel,
                'paySearchModel' => $paySearch,
                'payDataProvider' => $payDataProvider
            ]);
//        }
    }



    public function actionSendMessage()
    {
        $request = Yii::$app->request;
        $from = null;

        $model = new Chat([
            'author_id' => Yii::$app->user->identity->id,
            'created_at' => date('Y-m-d H:i')
        ]);

        if($model->load($request->post()) && $model->save()){
            return $this->renderAjax('chat-form', [
                'model' => Chat::findOne($model->contract_id),
            ]);
        }

        return [];
    }


    /**
     * Creates a new Contract model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Contract();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить проект",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose' => true,
                ];
            }else{           
                return [
                    'title'=> "Добавить проект",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Добавить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Contract model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $containerPjaxReload = '#crud-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить проект #" .$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>$containerPjaxReload,
                    'forceClose' => true,
                ];
            }else{
                 return [
                    'title'=> "Изменить проект #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Contract model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Contract model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Contract model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contract the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contract::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует');
        }
    }
}


