<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task_work_type".
 *
 * @property int $id
 * @property int $task_id id Задачи
 * @property int $work_type_id id типов работ
 *
 * @property Task $task
 * @property WorkType $workType
 */
class TaskWorkType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_work_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'work_type_id'], 'integer'],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['work_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => WorkType::className(), 'targetAttribute' => ['work_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_id' => 'id Задачи',
            'work_type_id' => 'id типов работ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkType()
    {
        return $this->hasOne(WorkType::className(), ['id' => 'work_type_id']);
    }

    /**
     * @inheritdoc
     * @return TaskWorkTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskWorkTypeQuery(get_called_class());
    }
}
