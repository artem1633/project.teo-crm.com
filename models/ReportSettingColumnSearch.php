<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReportSettingColumn;

/**
 * ReportSettingColumnSearch represents the model behind the search form about `app\models\ReportSettingColumn`.
 */
class ReportSettingColumnSearch extends ReportSettingColumn
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'contract_name', 'contract_customer_id', 'contract_contract_number', 'contract_contract_date', 'contract_contract_type', 'contract_contract_status', 'contract_city_id', 'contract_project_name', 'contract_contract_deadline', 'contract_contract_sum', 'contract_contract_signature_name', 'contract_contract_executor_name', 'contract_post_address', 'contract_scan_id', 'contract_comment', 'contract_author_id', 'contract_created_at', 'contract_updated_at', 'order_letter_date', 'order_letter_text', 'order_type', 'order_customer_id', 'order_author_id', 'order_city_id', 'order_object_number', 'order_object_address', 'order_order_number', 'order_order_date', 'order_fact_end_date', 'order_contract_id', 'order_order_deadline', 'order_order_status_id', 'order_expired', 'order_scan_added', 'order_related_orders', 'order_comment', 'order_construction_type', 'order_ams_type_id', 'order_placement_id', 'order_order_sum', 'order_created_at', 'order_updated_at', 'order_email_date', 'order_email_matching_date', 'order_edo_date', 'order_edo_getting_date', 'task_name', 'task_author_id', 'task_order_id', 'task_task_type', 'task_priority', 'task_task_status_id', 'task_responsible_id', 'task_task_deadline', 'task_checker_id', 'task_normocontroller_id', 'task_gip_id', 'task_work_sum', 'task_comment', 'task_task_desc', 'task_task_rating', 'task_payment_status', 'task_payment_balance', 'task_payment_debt', 'task_created_at', 'task_updated_at'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReportSettingColumn::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'contract_name' => $this->contract_name,
            'contract_customer_id' => $this->contract_customer_id,
            'contract_contract_number' => $this->contract_contract_number,
            'contract_contract_date' => $this->contract_contract_date,
            'contract_contract_type' => $this->contract_contract_type,
            'contract_contract_status' => $this->contract_contract_status,
            'contract_city_id' => $this->contract_city_id,
            'contract_project_name' => $this->contract_project_name,
            'contract_contract_deadline' => $this->contract_contract_deadline,
            'contract_contract_sum' => $this->contract_contract_sum,
            'contract_contract_signature_name' => $this->contract_contract_signature_name,
            'contract_contract_executor_name' => $this->contract_contract_executor_name,
            'contract_post_address' => $this->contract_post_address,
            'contract_scan_id' => $this->contract_scan_id,
            'contract_comment' => $this->contract_comment,
            'contract_author_id' => $this->contract_author_id,
            'contract_created_at' => $this->contract_created_at,
            'contract_updated_at' => $this->contract_updated_at,
            'order_letter_date' => $this->order_letter_date,
            'order_letter_text' => $this->order_letter_text,
            'order_type' => $this->order_type,
            'order_customer_id' => $this->order_customer_id,
            'order_author_id' => $this->order_author_id,
            'order_city_id' => $this->order_city_id,
            'order_object_number' => $this->order_object_number,
            'order_object_address' => $this->order_object_address,
            'order_order_number' => $this->order_order_number,
            'order_order_date' => $this->order_order_date,
            'order_fact_end_date' => $this->order_fact_end_date,
            'order_contract_id' => $this->order_contract_id,
            'order_order_deadline' => $this->order_order_deadline,
            'order_order_status_id' => $this->order_order_status_id,
            'order_expired' => $this->order_expired,
            'order_scan_added' => $this->order_scan_added,
            'order_related_orders' => $this->order_related_orders,
            'order_comment' => $this->order_comment,
            'order_construction_type' => $this->order_construction_type,
            'order_ams_type_id' => $this->order_ams_type_id,
            'order_placement_id' => $this->order_placement_id,
            'order_order_sum' => $this->order_order_sum,
            'order_created_at' => $this->order_created_at,
            'order_updated_at' => $this->order_updated_at,
            'order_email_date' => $this->order_email_date,
            'order_email_matching_date' => $this->order_email_matching_date,
            'order_edo_date' => $this->order_edo_date,
            'order_edo_getting_date' => $this->order_edo_getting_date,
            'task_name' => $this->task_name,
            'task_author_id' => $this->task_author_id,
            'task_order_id' => $this->task_order_id,
            'task_task_type' => $this->task_task_type,
            'task_priority' => $this->task_priority,
            'task_task_status_id' => $this->task_task_status_id,
            'task_responsible_id' => $this->task_responsible_id,
            'task_task_deadline' => $this->task_task_deadline,
            'task_checker_id' => $this->task_checker_id,
            'task_normocontroller_id' => $this->task_normocontroller_id,
            'task_gip_id' => $this->task_gip_id,
            'task_work_sum' => $this->task_work_sum,
            'task_comment' => $this->task_comment,
            'task_task_desc' => $this->task_task_desc,
            'task_task_rating' => $this->task_task_rating,
            'task_payment_status' => $this->task_payment_status,
            'task_payment_balance' => $this->task_payment_balance,
            'task_payment_debt' => $this->task_payment_debt,
            'task_created_at' => $this->task_created_at,
            'task_updated_at' => $this->task_updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
