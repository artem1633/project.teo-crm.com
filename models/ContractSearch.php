<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contract;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * ContractSearch represents the model behind the search form about `app\models\Contract`.
 */
class ContractSearch extends Contract
{


    public $dateStart;

    public $dateEnd;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'city_id',  'author_id'], 'integer'],
            ['contract_date', function(){
//                $dates = explode(' - ', $this->contract_date);
//                if(count($dates) < 2){
//                    $this->addError('contract_date', 'Некорректная дата');
//                    return false;
//                }
            }],
            [['contract_number','contract_status','userGroups',  'name', 'contract_date', 'contract_type', 'project_name', 'contract_deadline', 'post_address', 'comment', 'created_at', 'updated_at', 'dateStart', 'dateEnd'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contract::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->contract_status == null){
            $this->contract_status = 'В работе';
        }

        $sort = $dataProvider->getSort();

        $dataProvider->setSort([
            'defaultOrder' => [
                'id' => SORT_DESC,
            ],
            'attributes' => ArrayHelper::merge($sort->attributes, [
                'id',
                'country_id'
            ]),
        ]);



        $query->joinWith('groups');

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'contract_status' => $this->contract_status,
            'city_id' => $this->city_id,
            'contract_deadline' => $this->contract_deadline,
//            'contract_date' => $this->contract_date,
            'userGroups' => $this->userGroups,
            'author_id' => $this->author_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'contract_number', $this->contract_number])
            ->andFilterWhere(['like', 'contract_type', $this->contract_type])
            ->andFilterWhere(['like', 'project_name', $this->project_name])
            ->andFilterWhere(['like', 'post_address', $this->post_address])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        if($this->contract_date != null){
            $dates = explode(' - ', $this->contract_date);
            $dateStart = $dates[0];
            $dateEnd = $dates[1];

            $query->andFilterWhere(['between', 'contract_date', $dateStart, $dateEnd]);
        }

//
//        if(Yii::$app->user->identity->can('contract_view') && Yii::$app->user->identity->can('contract_view_all') == false){
//            $query->andFilterWhere(['or', ['city_id' => Yii::$app->user->identity->city_id], ['author_id' => Yii::$app->user->identity->getId()]]);
//        }

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $query->andWhere(['or', ['author_id' => Yii::$app->user->getId()], ['contract_user_group.user_id' => Yii::$app->user->getId()]]);
        }

        $query->groupBy('contract.id');

        return $dataProvider;
    }
}
