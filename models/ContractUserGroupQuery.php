<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ContractUserGroup]].
 *
 * @see ContractUserGroup
 */
class ContractUserGroupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ContractUserGroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ContractUserGroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
