<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[OrderWorkType]].
 *
 * @see OrderWorkType
 */
class OrderWorkTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return OrderWorkType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OrderWorkType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
