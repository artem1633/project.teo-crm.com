<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'author_id', 'city_id', 'contract_id', 'expired', 'scan_added', 'related_orders', 'order_status_id', 'hours_plane','hours_fact'], 'integer'],
            [['name', 'order_date', 'order_deadline',  'comment', 'fact_end_date','created_at','updated_at','order_sum','order_sum',], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('orderUserGroup');

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'author_id' => $this->author_id,
            'order_status_id' => $this->order_status_id,
            'city_id' => $this->city_id,
            'order_date' => $this->order_date,
            'contract_id' => $this->contract_id,
            'order_deadline' => $this->order_deadline,
            'expired' => $this->expired,
            'scan_added' => $this->scan_added,
            'related_orders' => $this->related_orders,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'hours_fact', $this->hours_fact])
            ->andFilterWhere(['like', 'hours_plane', $this->hours_plane])
            ->andFilterWhere(['like', 'order_sum', $this->order_sum]);

        if(Yii::$app->user->identity->can('order_view') && Yii::$app->user->identity->can('order_view_all') == false){
            $query->andFilterWhere(['author_id' => Yii::$app->user->identity->getId()]);
        }

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $query->orWhere(['or', ['author_id' => Yii::$app->user->getId()], ['order_user_group.user_id' => Yii::$app->user->getId()]]);
        }

        $query->groupBy('order.id');

        return $dataProvider;
    }
}
