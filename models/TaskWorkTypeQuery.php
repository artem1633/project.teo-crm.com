<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TaskWorkType]].
 *
 * @see TaskWorkType
 */
class TaskWorkTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TaskWorkType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TaskWorkType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
