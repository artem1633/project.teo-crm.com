<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Subgroups]].
 *
 * @see Subgroups
 */
class SQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Subgroups[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Subgroups|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
