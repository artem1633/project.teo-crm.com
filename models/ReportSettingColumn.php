<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "report_setting_column".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $user_id Пользователь
 * @property int $contract_name
 * @property int $contract_customer_id
 * @property int $contract_contract_number
 * @property int $contract_contract_date
 * @property int $contract_contract_type
 * @property int $contract_contract_status
 * @property int $contract_city_id
 * @property int $contract_project_name
 * @property int $contract_contract_deadline
 * @property int $contract_contract_sum
 * @property int $contract_contract_signature_name
 * @property int $contract_contract_executor_name
 * @property int $contract_post_address
 * @property int $contract_scan_id
 * @property int $contract_comment
 * @property int $contract_author_id
 * @property int $contract_created_at
 * @property int $contract_updated_at
 * @property int $order_letter_date
 * @property int $order_letter_text
 * @property int $order_type
 * @property int $order_customer_id
 * @property int $order_author_id
 * @property int $order_city_id
 * @property int $order_object_number
 * @property int $order_object_address
 * @property int $order_order_number
 * @property int $order_order_date
 * @property int $order_fact_end_date
 * @property int $order_contract_id
 * @property int $order_order_deadline
 * @property int $order_order_status_id
 * @property int $order_expired
 * @property int $order_scan_added
 * @property int $order_related_orders
 * @property int $order_comment
 * @property int $order_construction_type
 * @property int $order_ams_type_id
 * @property int $order_placement_id
 * @property int $order_order_sum
 * @property int $order_created_at
 * @property int $order_updated_at
 * @property int $order_email_date
 * @property int $order_email_matching_date
 * @property int $order_edo_date
 * @property int $order_edo_getting_date
 * @property int $task_name
 * @property int $task_author_id
 * @property int $task_order_id
 * @property int $task_task_type
 * @property int $task_priority
 * @property int $task_task_status_id
 * @property int $task_responsible_id
 * @property int $task_task_deadline
 * @property int $task_checker_id
 * @property int $task_normocontroller_id
 * @property int $task_gip_id
 * @property int $task_work_sum
 * @property int $task_comment
 * @property int $task_task_desc
 * @property int $task_task_rating
 * @property int $task_payment_status
 * @property int $task_payment_balance
 * @property int $task_payment_debt
 * @property int $task_created_at
 * @property int $task_updated_at
 *
 * @property User $user
 */
class ReportSettingColumn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_setting_column';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'user_id',
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'contract_name', 'contract_customer_id', 'contract_contract_number', 'contract_contract_date', 'contract_contract_type', 'contract_contract_status', 'contract_city_id', 'contract_project_name', 'contract_contract_deadline', 'contract_contract_sum', 'contract_contract_signature_name', 'contract_contract_executor_name', 'contract_post_address', 'contract_scan_id', 'contract_comment', 'contract_author_id', 'contract_created_at', 'contract_updated_at', 'order_letter_date', 'order_letter_text', 'order_type', 'order_customer_id', 'order_author_id', 'order_city_id', 'order_object_number', 'order_object_address', 'order_order_number', 'order_order_date', 'order_fact_end_date', 'order_contract_id', 'order_order_deadline', 'order_order_status_id', 'order_expired', 'order_scan_added', 'order_related_orders', 'order_comment', 'order_construction_type', 'order_ams_type_id', 'order_placement_id', 'order_order_sum', 'order_created_at', 'order_updated_at', 'order_email_date', 'order_email_matching_date', 'order_edo_date', 'order_edo_getting_date', 'order_workTypes', 'task_name', 'task_author_id', 'task_order_id', 'task_task_type', 'task_priority', 'task_task_status_id', 'task_responsible_id', 'task_task_deadline', 'task_checker_id', 'task_normocontroller_id', 'task_gip_id', 'task_work_sum', 'task_comment', 'task_task_desc', 'task_task_rating', 'task_payment_status', 'task_payment_balance', 'task_payment_debt', 'task_created_at', 'task_updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $output = [
            'id' => 'ID',
            'name' => 'Наименование',
            'user_id' => 'Пользователь',
            'order_workTypes' => 'Типы работ',
        ];

        foreach (array_keys($this->attributes) as $attr)
        {
            if($attr == 'name' || $attr == 'user_id' || $attr == 'order_workTypes'){
                continue;
            }

            $arr = explode('_', $attr);
            $model = $arr[0];

            if($model == 'contract'){
                $model = new Contract();
            } else if($model == 'order'){
                $model = new Order();
            } else if($model == 'task'){
                $model = new Task();
            } else {
                continue;
            }

            $attribute = implode('_', array_slice($arr, 1));

            $output[$attr] = $model->getAttributeLabel($attribute);
        }

        return $output;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return array
     */
    public function getColumnsAttributes(){
        return [
            'id',
            'name',
            'user_id',
            'contract_customer_id',
            'contract_contract_number',
            'contract_contract_date',
            'contract_contract_type',
            'contract_contract_status',
            'contract_city_id',
//            'contract_name',
            'contract_project_name',
            'contract_contract_deadline',
            'contract_contract_sum',
            'contract_contract_signature_name',
            'contract_contract_executor_name',
            'contract_post_address',
            'contract_scan_id',
            'contract_comment',
            'contract_author_id',
            'contract_created_at',
            'contract_updated_at',
            'order_contract_id',
            'order_order_status_id',
            'order_email_date',
            'order_email_matching_date',
            'order_edo_date',
            'order_edo_getting_date',
            'order_letter_date',
            'order_letter_text',
            'order_order_number',
            'order_order_date',
            'order_object_number',
            'order_object_address',
            'order_order_deadline',
            'order_fact_end_date',
            'order_construction_type',
            'order_ams_type_id',
            'order_placement_id',
            'order_comment',
            'order_order_sum',
//            'order_type',
//            'order_customer_id',
//            'order_author_id',
//            'order_city_id',
//            'order_expired',
//            'order_scan_added',
//            'order_related_orders',
            'order_created_at',
            'order_updated_at',
//            'task_name',
            'task_order_id',
//            'task_workKinds',
            'task_task_desc',
            'task_responsible_id',
            'task_checker_id',
            'task_normocontroller_id',
            'task_gip_id',
            'task_task_deadline',
            'task_priority',
            'task_work_sum',
            'task_task_status_id',
            'task_comment',
//            'task_author_id',
//            'task_task_type',
//            'task_task_rating',
//            'task_payment_status',
//            'task_payment_balance',
//            'task_payment_debt',
            'task_created_at',
            'task_updated_at',
//            'order_workTypes',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getColumns()
    {
        $columns = [];
//        foreach (array_keys($this->attributes) as $attr){
        foreach ($this->getColumnsAttributes() as $attr){
            if($attr == 'id' || $attr == 'name' || $attr == 'user_id' || $attr == 'contract_scan_id'){
                continue;
            }

            if($this->$attr != null){
                $arr = explode('_', $attr);
                if($arr[0] == 'task'){
                    $str = 'task_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Задача</span>';
                } else if($arr[0] == 'order'){
                    $str = implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-danger">Заказ</span>';
                } else {
                    $str = $arr[0].'_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-warning">Договор</span>';
                }
                $columns[] = [
                    'attribute' => $str,
                    'header' => "<div style='width: 100%; text-align: center;'>{$header}</div>".$this->getAttributeLabel($attr),
                ];
            }
        }

//        $columns[] = [
//            'attribute' => null,
//            'header' => '<div style=\'width: 100%; text-align: center;\'><span class="label label-danger">Заказ</span></div>Типы работ'
//        ];

//        VarDumper::dump($columns, 10, true);
//        exit;

        return $columns;
    }
}
