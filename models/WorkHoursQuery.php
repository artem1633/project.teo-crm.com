<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[WorkHours]].
 *
 * @see WorkHours
 */
class WorkHoursQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return WorkHours[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return WorkHours|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
