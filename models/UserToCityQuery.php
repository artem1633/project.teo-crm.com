<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserToCity]].
 *
 * @see UserToCity
 */
class UserToCityQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return UserToCity[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserToCity|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
