<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subtask".
 *
 * @property int $id
 * @property string $desc Текст подзадачи
 * @property int $task_id связь с задачей
 * @property int $status статус выполнения
 *
 * @property Task $task
 */
class Subtask extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subtask';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['desc'], 'string'],
            [['task_id', 'status'], 'integer'],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'desc' => 'Текст подзадачи',
            'task_id' => 'связь с задачей',
            'status' => 'статус выполнения',
        ];
    }
    public function beforeSave($insert)
    {
        if($insert){
            (new TaskHistory([
                'user_id' => Yii::$app->user->identity->id,
                'task_id' => $this->task_id,
                'history_comment' => "Была создана подзадача {$this->desc}" ,
                'created_at' => date('Y-m-d H:i:s'),
            ]))->save(false);
        }
        return parent::beforeSave($insert);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @inheritdoc
     * @return SubtaskQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SubtaskQuery(get_called_class());
    }
}
