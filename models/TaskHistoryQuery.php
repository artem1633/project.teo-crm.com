<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TaskHistory]].
 *
 * @see TaskHistory
 */
class TaskHistoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TaskHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TaskHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
