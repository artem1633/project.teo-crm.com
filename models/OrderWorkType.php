<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_work_type".
 *
 * @property int $id
 * @property int $order_id id Заказа
 * @property int $work_type_id id типов работ
 *
 * @property Task $order
 * @property WorkType $workType
 */
class OrderWorkType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_work_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'work_type_id'], 'integer'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['work_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => WorkType::className(), 'targetAttribute' => ['work_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'id Заказа',
            'work_type_id' => 'id типов работ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Task::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkType()
    {
        return $this->hasOne(WorkType::className(), ['id' => 'work_type_id']);
    }

    /**
     * @inheritdoc
     * @return OrderWorkTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderWorkTypeQuery(get_called_class());
    }
}
