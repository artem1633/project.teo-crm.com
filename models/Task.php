<?php

namespace app\models;

use app\components\MyUploadedFile;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property int $order_id Объект Задачи
 * @property int $author_id автор
 * @property string $task_type Дата задачи
 * @property string $priority Приоритет
 * @property integer $task_status_id Статус задачи
 * @property int $responsible_id Отвественный
 * @property string $task_deadline Срок Задачи
 * @property int $checker_id Проверящий
 * @property int $name Название

 * @property string $work_sum Стоимость работ
 * @property string $payment_balance Оплачено
 * @property string $payment_debt Остаток оплаты
 * @property string $review Отзы о работе
 * @property string $hours_plane часов план
 * @property int $hours_fact Часов факт
 * @property string $payment_type часов план

 * @property User $checker


 * @property Order $order
 * @property User $responsible
 * * @property TaskWorkType[] $taskWorkTypes
 */
class Task extends \yii\db\ActiveRecord
{
    public $userGroup;
    public $workTypes;
    public $listFile;
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'responsible_id', 'checker_id',  'task_rating','author_id', 'task_status_id','hours_plane','hours_fact'], 'integer'],
            [['task_type', 'task_deadline','workTypes','listFile','created_at', 'updated_at', 'payment_balance','task_desc', 'payment_debt', 'userGroup'], 'safe'],
            [['priority', 'work_sum',  'name', 'payment_status','review','payment_type'], 'string', 'max' => 255],
            [['checker_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['checker_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['task_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskStatus::className(), 'targetAttribute' => ['task_status_id' => 'id']],
            [['responsible_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['responsible_id' => 'id']],
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => null,
                'createdByAttribute' => 'author_id',
                'value' => Yii::$app->user->id
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_id' => 'Автор',
            'order_id' => 'Объект Задачи',
            'task_type' => 'Тип задачи',
            'priority' => 'Приоритет',
            'task_status_id' => 'Статус задачи',
            'responsible_id' => 'Исполнитель',
            'task_deadline' => 'Срок выполнения',
            'checker_id' => 'Проверящий',
            'payment_type'=> 'Тип оплаты',
            'hours_plane' => 'Часов план',
            'hours_fact' => 'Часов факт',
            'order_sum' => 'Сумма заказа',
            'work_sum' => 'Стоимость работ',
            'userGroup' => 'Группа пользователей',
            'name' => 'Название',
            'task_desc' => 'Описание задачи',
            'task_rating' => 'Оценка работы',
            'payment_status' => 'Статус оплаты',
            'payment_balance' => 'Оплачено',
            'payment_debt' => 'Остаток оплаты',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'workTypes' => 'Ключевики',
            'review' => 'Отзыв о работе'
        ];
    }
    public function beforeDelete()
    {
        $count = Order::find()->where(['id' => $this->order_id])->count();
        if($count > 0){
            return false;
        }
        TaskHistory::deleteAll(['task_id' => $this->id]);



        return parent::beforeDelete();
    }
    public function beforeSave($insert)
    {
        if (!$this->isNewRecord) {
            $last =Task::findOne($this->id);
            $this->setChanging($last, $this, false);
        }
        if (parent::beforeSave($insert)) {

            //... тут ваш код

            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert){
            (new TaskHistory([
                'user_id' => Yii::$app->user->identity->id,
                'task_id' => $this->id,
                'history_comment' => 'Была создана задача',
                'created_at' => date('Y-m-d H:i:s'),
            ]))->save(false);
        }

        if ($this->workTypes != null) {
            $up = false;
            $allSubs = TaskWorkType::find()->where(['task_id' => $this->id])->all();
            foreach ($allSubs as $sub) {
                if (array_search($sub, $this->workTypes) !== false) {
                    continue;

                } else {
                    $sub->delete();
                }
            }

            foreach ($this->workTypes as $subgroup) {

                $subgroup2 = TaskWorkType::find()->where(['task_id' => $this->id, 'work_type_id' => $subgroup])->one();
                if (!$subgroup2) {
                    $up = true;
                    (new TaskWorkType([
                        'work_type_id' => $subgroup,
                        'task_id' => $this->id
                    ]))->save(false);
                }
            }
            if($up == true and $this->isNewRecord == false){
                $a = ArrayHelper::getColumn($allSubs, 'work_type_id');
                $lost = ArrayHelper::getColumn(WorkType::find()->where(['id' => $a])->all(), 'name');
                $a2 = ArrayHelper::getColumn(TaskWorkType::find()->where(['task_id' => $this->id])->all(), 'work_type_id');
                $new = ArrayHelper::getColumn(WorkType::find()->where(['id' => $a2])->all(), 'name');
                $this->setToChangeTable('Ключевики: ',implode(', ', $lost),implode(', ', $new),'измнены');
            }
        }

        if($this->listFile != null){
            $files = MyUploadedFile::getInstancesByName('listFile', true);

            $files = array_combine(ArrayHelper::getColumn($this->listFile, 'name'), $files);
            foreach ($files as $name => $file){
                /** @var $file MyUploadedFile */

                $path = Yii::$app->security->generateRandomString();
                $path = "uploads/{$path}.$file->extension";

                $file->saveAs($path);

                $scan = new Scan([
                    'name' => $name,
                    'link' => $path,
                    'task_id' => $this->id,
                ]);
                $scan->save(false);
            }
        }


        TaskUserGroup::deleteAll(['task_id' => $this->id]);
        if($this->userGroup){
            foreach ($this->userGroup as $group){
                (new TaskUserGroup([
                    'task_id' => $this->id,
                    'user_id' => $group,
                ]))->save(false);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskWorkTypes()
    {
        return $this->hasMany(TaskWorkType::className(), ['task_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChecker()
    {
        return $this->hasOne(User::className(), ['id' => 'checker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGip()
    {
        return $this->hasOne(User::className(), ['id' => 'gip_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNormocontroller()
    {
        return $this->hasOne(User::className(), ['id' => 'normocontroller_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskUserGroup()
    {
        return $this->hasMany(TaskUserGroup::className(), ['task_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsible()
    {
        return $this->hasOne(User::className(), ['id' => 'responsible_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskStatus()
    {
        return $this->hasOne(TaskStatus::className(), ['id' => 'task_status_id']);
    }

    /**
     * @inheritdoc
     * @return TaskQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskQuery(get_called_class());
    }

    public function Change($last, $new)
    {
        $result = "";
//
//        if($last->author_id != $new->author_id) $result .= "Автор =>".$new->author_id->name."%0A";
        if($last->task_deadline != $new->task_deadline) $result .= 'Срок Задачи => '. $new->task_deadline."%0A";
        if($last->task_status_id != $new->task_status_id) $result .= 'Статус задачи => '. $new->task_status_id->name."%0A";
        if($last->order_id != $new->order_id) $result .= 'Объект задачи => '. $new->order_id->name."%0A";
        if($last->work_sum != $new->work_sum) $result .= 'Стоимость работ => '. $new->work_sum."%0A";
        if($last->task_type != $new->task_type) $result .= 'Тип задачи => '. $new->task_type."%0A";
        if($last->workTypes != $new->workTypes) $result .= 'Ключевики => '. $new->workTypes->name."%0A";
        if($last->priority != $new->priority) $result .= 'Проритет => '. $new->priority."%0A";
        if($last->task_desc != $new->task_desc) $result .= 'Описание задачи => '. $new->task_desc."%0A";
        if($last->task_rating != $new->task_rating) $result .= 'Оценка => '. $new->task_rating."%0A";
        if($last->payment_status != $new->payment_status) $result .= 'Статус оплаты => '. $new->payment_status."%0A";
        if($last->responsible_id != $new->responsible_id) $result .= 'Отвественный => '. $new->responsible_id->name."%0A";
        if($last->checker_id != $new->checker_id) $result .= 'Проверяющий => '. $new->checker_id->name."%0A";
        if($last->hours_fact != $new->hours_fact) $result .= 'Часов Факт => '. $new->hours_fact."%0A";
        if($last->hours_plane != $new->hours_plane) $result .= 'Часов план => '. $new->hours_plane."%0A";
        if($last->name != $new->name) $result .= ' Название задачи => '. $new->name."%0A";
        if($last->review != $new->review) $result .= 'Отзыв о работе => '. $new->review."%0A";
        if($last->listfile != $new->listfaie) $result .= 'Загрузка документа => '. $new->file->name."%0A";
        /*'file' => 'Файл',
        'commentary' => 'Комментарии',*/
        return $result;
    }

    public function setChanging($old, $new, $status)
    {
        if($status == "Изменено"){

//            $this->setToChangeTable('Автор',"",$new->author_id->name,$status);
            $this->setToChangeTable('Объект Задачи',"",$new->order_id->name,$status);
            $this->setToChangeTable('Срок Задачи',"",$new->task_deadline,$status);
            $this->setToChangeTable('Стоимость работ',"",$new->work_sum,$status);
            $this->setToChangeTable('Тип задачи',"",$new->task_type,$status);
            $this->setToChangeTable('Ключевики',"",$new->workTypes,$status);
            $this->setToChangeTable('Приоритет',"",$new->priority,$status);
            $this->setToChangeTable('Статус задачи',"",$new->task_status_id->name,$status);
            $this->setToChangeTable('Описание задачи',"",$new->task_desc,$status);
            $this->setToChangeTable('Оценка',"",$new->task_rating,$status);
            $this->setToChangeTable('Статус оплаты',"",$new->payment_status,$status);
            $this->setToChangeTable('Отвественный',"",$new->responsible_id->name,$status);
            $this->setToChangeTable('Проверяющй',"",$new->checker_id->name,$status);
            $this->setToChangeTable('Часов план',"",$new->hours_plane,$status);
            $this->setToChangeTable('Часоов факт',"",$new->hours_fact,$status);
             $this->setToChangeTable('Название задчи',"",$new->name,$status);
            $this->setToChangeTable('Загрузка документа',"",$new->listfaie,$status);
            $this->setToChangeTable('Отзыв о работе',"",$new->review,$status);
        }else{
//             if($old->author_id != $new->author_id)
//                $this->setToChangeTable('Автор',$old->author_id->name,$new->author->name,$status);
            if($old->order_id != $new->order_id){
                $oldval = Order::find()->where(['id' => $old->order_id])->one();
                $newval = Order::find()->where(['id' => $new->order_id])->one();
                if ($oldval == null){
                    $this->setToChangeTable('Объект задачи',' ',$newval->name,$status);
                }else{
                    $this->setToChangeTable('Объект задачи',$oldval->name,$newval->name,$status);
                }
            }
            if($old->hours_plane != $new->hours_plane)
                $this->setToChangeTable('Часов план',$old->hours_plane,$new->hours_plane,$status);
            if($old->hours_fact != $new->hours_fact)
                $this->setToChangeTable('Часов факт',$old->hours_fact,$new->hours_fact,$status);
            if($old->review != $new->review)
                $this->setToChangeTable('Отзыв о работе',$old->review,$new->review,$status);
            if($old->task_deadline != $new->task_deadline)
                $this->setToChangeTable('Срок сдачи',$old->task_deadline,$new->task_deadline,$status);
            if($old->work_sum != $new->work_sum)
                $this->setToChangeTable('Стоимость работ',$old->work_sum,$new->work_sum,$status);
            if($old->task_type != $new->task_type)
                $this->setToChangeTable('Тип задачи',$old->task_type,$new->task_type,$status);
            if($old->priority != $new->priority)
                $this->setToChangeTable('Приоритет',$old->priority,$new->priority,$status);
            if($old->task_status_id != $new->task_status_id){
                $oldval = TaskStatus::find()->where(['id' => $old->task_status_id])->one();
                $newval = TaskStatus::find()->where(['id' => $new->task_status_id])->one();
                if ($oldval == null){
                    $this->setToChangeTable('Статус задачи',' ',$newval->name,$status);
                }else{
                $this->setToChangeTable('Статус задачи',$oldval->name,$newval->name,$status);
                }
            }
            if($old->task_desc != $new->task_desc)
                $this->setToChangeTable('Описание задачи',$old->task_desc,$new->task_desc,$status);
            if($old->task_rating != $new->task_rating)
                $this->setToChangeTable('Оценка',$old->task_rating,$new->task_rating,$status);
            if($old->payment_status != $new->payment_status)
                $this->setToChangeTable('Статус оплаты',$old->payment_status,$new->payment_status,$status);
            if($old->responsible_id != $new->responsible_id){
                $oldval = User::find()->where(['id' => $old->responsible_id])->one();
                $newval = User::find()->where(['id' => $new->responsible_id])->one();
                if ($oldval == null){
                    $this->setToChangeTable('Отвественный',' ',$newval->name,$status);
                }else{
                    $this->setToChangeTable('Отвественный',$oldval->name,$newval->name,$status);
                }
            }
            if($old->checker_id != $new->checker_id){
                $oldval = User::find()->where(['id' => $old->checker_id])->one();
                $newval = User::find()->where(['id' => $new->checker_id])->one();
                if ($oldval == null){
                    $this->setToChangeTable('Проверяющий',' ',$newval->name,$status);
                }else{
                    $this->setToChangeTable('Проверяющий',$oldval->name,$newval->name,$status);
                }
            }
            if($old->name != $new->name)
                $this->setToChangeTable('Название задачи',$old->name,$new->name,$status);

            if($old->listFile != $new->listFile) {
                $info = '';
                foreach ($new->listFile as $item) {
                    $info .= $item['name'].', ';
                }
                (new TaskHistory([
                    'user_id' => Yii::$app->user->identity->id,
                    'task_id' => $this->id,
                    'history_comment' => 'Были загружены документы: '.$info,
                    'created_at' => date('Y-m-d H:i:s'),
                ]))->save(false);
            }

        }

    }
    public function setToChangeTable($field,$old_value,$new_value,$status)
    {
        $model = new TaskHistory();
        $model->task_id = $this->id;
        $model->created_at = date('Y-m-d H:i:s');
        $model->user_id = Yii::$app->user->identity->id;
        $model->history_comment = "Были изменения в '{$field}' с '{$old_value}' на '{$new_value}'";

        $model->save();
    }
}
