<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "task_status".
 *
 * @property int $id
 * @property string $name Название
 * @property int $state Исходное состояние
 * @property int $successful Успешное завершение
 * @property int $unsuccessful Неуспешное завершение
 * @property string $color Цвет
 *
 * @property Task[] $tasks
 */
class TaskStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state', 'successful', 'unsuccessful'], 'integer'],
            [['name', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'state' => 'Исходное состояние',
            'successful' => 'Успешное завершение',
            'unsuccessful' => 'Неуспешное завершение',
            'color' => 'Цвет',
        ];
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['task_status_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return TaskStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskStatusQuery(get_called_class());
    }
}
