<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_history".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $order_id Id Заказка
 * @property string $history_comment Содержание действия
 * @property string $created_at
 *
 * @property Order $order
 * @property User $user
 */
class OrderHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'order_id'], 'integer'],
            [['history_comment'], 'string'],
            [['created_at'], 'safe'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'order_id' => 'Id Заказка',
            'history_comment' => 'Содержание действия',
            'created_at' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return OrderHistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderHistoryQuery(get_called_class());
    }
}
