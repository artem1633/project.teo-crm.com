<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task;

/**
 * TaskSearch represents the model behind the search form about `app\models\Task`.
 */
class TaskSearch extends Task
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'responsible_id', 'checker_id','task_rating','author_id', 'task_status_id', 'hours_plane','hours_fact'], 'integer'],
            [['task_type','created_at', 'updated_at', 'priority', 'task_deadline', 'work_sum', 'workTypes',  'name', 'task_desc', 'payment_status','payment_type', 'payment_balance', 'payment_debt','review'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('taskUserGroup');

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'task_type' => $this->task_type,
            'responsible_id' => $this->responsible_id,
            'task_deadline' => $this->task_deadline,
            'task_status_id' => $this->task_status_id,
            'checker_id' => $this->checker_id,

            'author_id' => $this->author_id,
            'workTypes' => $this->workTypes,
            'review' =>  $this->review
        ]);

        $query->andFilterWhere(['like', 'priority', $this->priority])
              ->andFilterWhere(['like', 'work_sum', $this->work_sum])
              ->andFilterWhere(['like', 'payment_balance', $this->payment_balance])
              ->andFilterWhere(['like', 'payment_type', $this->payment_balance])
              ->andFilterWhere(['like', 'hours_fact', $this->hours_fact])
              ->andFilterWhere(['like', 'hours_plane', $this->hours_plane])
              ->andFilterWhere(['like', 'payment_debt', $this->payment_debt]);

        if(Yii::$app->user->identity->can('task_view') && Yii::$app->user->identity->can('task_view_all') == false){
            $query->andFilterWhere([
                'or',
                ['author_id' => Yii::$app->user->identity->getId()],
//                ['gip_id' => Yii::$app->user->identity->getId()],
                ['checker_id' => Yii::$app->user->identity->getId()],
                ['responsible_id' => Yii::$app->user->identity->getId()],
            ]);
        }

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $query->orWhere(['or', ['author_id' => Yii::$app->user->getId()], ['task_user_group.user_id' => Yii::$app->user->getId()]]);
        }


        return $dataProvider;
    }
}
