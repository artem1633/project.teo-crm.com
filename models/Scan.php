<?php

namespace app\models;

use app\components\MyUploadedFile;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "scan".
 *
 * @property int $id
 * @property string $name название
 * @property string $loaded_at дата и время загрузки
 * @property string $link ссылка на файл
 * @property int $author_id кто загрузил
 * @property int $contract_id кто загрузил
 * @property int $order_id кто загрузил
 *  @property int $task_id кто загрузил
 * @property int $user_id кто загрузил
 * @property int $agreement_id

 *
 * @property Contract[] $contracts
 * @property User $author
 */
class Scan extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $listFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scan';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'loaded_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'author_id',
                'value' => Yii::$app->user->id
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loaded_at', 'listFile'], 'safe'],
            [['author_id'], 'integer'],
            [['name', 'link'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название документа',
            'loaded_at' => 'Дата загрузки',
            'link' => 'Ссылка на документ',
            'author_id' => 'Пользователь загрузивший документ',
            'contract_id' => 'Пользователь загрузивший документ',
            'order_id' => 'Пользователь загрузивший документ',
            'task_id' => 'Пользователь загрузивший документ',
            'user_id' => 'Пользователь загрузивший документ',
            'agreement_id' => 'файл'
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->listFile != null){
            $files = MyUploadedFile::getInstancesByName('listFile', true);

            $files = array_combine(ArrayHelper::getColumn($this->listFile, 'name'), $files);
            foreach ($files as $name => $file){
                /** @var $file MyUploadedFile */

                $path = Yii::$app->security->generateRandomString();
                $path = "uploads/{$path}.$file->extension";

                $file->saveAs($path);

                $scan = new Scan([
                    'name' => $name,
                    'link' => $path,
                    'contract_id' => $this->contract_id,
                    'order_id' => $this->order_id,
                    'task_id' => $this->task_id,
                    'user_id' => $this->user_id,
                    'author_id' => Yii::$app->user->getId(),
                ]);
                $scan->save(false);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        if($this->contract_id != null){
                (new History([
                    'user_id' => Yii::$app->user->identity->id,
                    'contract_id' => $this->contract_id,
                    'history_comment' => 'Был удален документ: '.$this->name,
                    'created_at' => date('Y-m-d H:i:s'),
                ]))->save(false);
        }
        if($this->order_id != null){
            (new OrderHistory([
                'user_id' => Yii::$app->user->identity->id,
                'order_id' => $this->order_id,
                'history_comment' => 'Был удален документ: '.$this->name,
                'created_at' => date('Y-m-d H:i:s'),
            ]))->save(false);
        }
        if($this->task_id != null){
            (new TaskHistory([
                'user_id' => Yii::$app->user->identity->id,
                'task_id' => $this->task_id,
                'history_comment' => 'Был удален документ: '.$this->name,
                'created_at' => date('Y-m-d H:i:s'),
            ]))->save(false);
        }

        if($this->agreement_id != null){
            $agreement = Agreement::findOne($this->agreement_id);
            $contract = Contract::findOne($agreement->contract_id);
            if($agreement && $contract){
                (new History([
                    'user_id' => Yii::$app->user->identity->id,
                    'contract_id' => $contract->id,
                    'history_comment' => 'Был удален файл '.$this->name.' из Д/С '.$agreement->name,
                    'created_at' => date('Y-m-d H:i:s'),
                ]))->save(false);
            }
        }

        parent::afterDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContracts()
    {
        return $this->hasMany(Contract::className(), ['scan_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @inheritdoc
     * @return ScanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ScanQuery(get_called_class());
    }
}
