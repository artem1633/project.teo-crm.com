<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_contact".
 *
 * @property int $id
 * @property string $name ФИО контактного лица
 * @property string $position должность контактного лица
 * @property string $phone телефон контактного лица
 * @property string $duty_phone служебный тел. контактного лица
 * @property string $email почта контактного лица
 * @property int $customer_id котрагент
 *
 * @property Customer $customer
 */
class CustomerContact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'integer'],
            [['name', 'position', 'phone', 'duty_phone', 'email'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'position' => 'Должность',
            'phone' => 'Телефон',
            'duty_phone' => 'Служебный телефон',
            'email' => 'Email',
            'customer_id' => 'Customer ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @inheritdoc
     * @return CustomerContactQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CustomerContactQuery(get_called_class());
    }
}
