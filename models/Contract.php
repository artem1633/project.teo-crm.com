<?php

namespace app\models;
use app\components\MyUploadedFile;
use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "contract".
 *
 * @property int $id
 * @property string $name название проекта
 * @property int $customer_id связь с клиентом
 * @property string $contract_number номер проекта
 * @property string $contract_date дата проекта
 * @property string $contract_type вид проекта
 * @property string $contract_status статус проекта
 * @property int $city_id город проекта
 * @property string $project_name название проекта
 * @property string $contract_deadline скрок окончания проекта

 * @property string $post_address почтовый адрес

 * @property string $comment комментарий
 * @property int $author_id кто создал
 * @property string $created_at дата созданя
 * @property string $updated_at дата изменения
 *
 * @property User $author
 * @property City $city
 * * @property User $userGroups
 * @property Customer $customer
 * @property Scan $scan
 * * * @property ContractUserGroup[] $groups

 */
class Contract extends \yii\db\ActiveRecord
{

    public $listFile;
    public $file;
    public $userGroups;

    public $customerName;
    public $cityName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contract_number', 'contract_date'], 'required'],
            [['customer_id', 'city_id', 'author_id'], 'integer'],
            [['contract_date', 'contract_deadline', 'created_at', 'updated_at','listFile', 'userGroups'], 'safe'],
            [['name', 'contract_number', 'contract_status', 'contract_type', 'project_name', 'post_address', 'comment'], 'string', 'max' => 255],
            [['customer_id', 'city_id', 'author_id'], 'integer'],
            [['contract_date', 'contract_deadline', 'created_at', 'updated_at','workTypes','listFile', ], 'safe'],
            [['name', 'contract_number', 'contract_status', 'contract_type', 'project_name', 'post_address', 'comment', 'customerName', 'cityName'], 'string', 'max' => 255],

            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
//            [['scan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Scan::className(), 'targetAttribute' => ['scan_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название проекта',
            'customer_id' => 'Контрагент',
            'customerName' => 'Контрагент',
            'contract_number' => 'Номер договора',
            'contract_date' => 'Дата проекта',
            'contract_type' => 'Вид проекта',
            'contract_status' => 'Статус проекта',
            'city_id' => 'Город',
            'cityName' => 'Город',
            'project_name' => 'Название проекта',
            'contract_deadline' => 'Срок окончания проекта',
            'post_address' => 'Почтовый адрес',
            'comment' => 'Комментарий',
            'author_id' => 'Автор',
            'created_at' => 'Дата созадния',
            'updated_at' => 'Дата изменения',
            'userGroups' => 'Группа пользователей'

        ];
    }

    public function beforeDelete()
    {
        History::deleteAll(['contract_id' => $this->id]);

        return parent::beforeDelete();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!$this->isNewRecord) {
            $last = Contract::findOne($this->id);
            $this->setChanging($last, $this, false);
        }
        if (parent::beforeSave($insert)) {

            if($this->customerName){
                $customer = Customer::find()->where(['name' => $this->customerName])->one();

                if($customer){
                    if($customer->id != $this->customer_id){
                        $this->customer_id = $customer->id;
                    }
                } else {
                    $customer = new Customer(['name' => $this->customerName]);
                    $customer->save(false);
                    $this->customer_id = $customer->id;
                }
            }

            if($this->cityName){
                $city = City::find()->where(['name' => $this->cityName])->one();

                if($city){
                    if($city->id != $this->city_id){
                        $this->city_id = $city->id;
                    }
                } else {
                    $city = new City(['name' => $this->cityName]);
                    $city->save(false);
                    $this->city_id = $city->id;
                }
            }

            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert){
            (new History([
                'user_id' => Yii::$app->user->identity->id,
                'contract_id' => $this->id,
                'history_comment' => 'Был создан договор',
                'created_at' => date('Y-m-d H:i:s'),
            ]))->save(false);
        }

        if ($this->userGroups != null) {
            $up = false;
            $allSubs = ContractUserGroup::find()->where(['contract_id' => $this->id])->all();
            foreach ($allSubs as $sub) {
                if (array_search($sub, $this->userGroups) !== false) {
                    continue;

                } else {
                    $sub->delete();
                }
            }

            foreach ($this->userGroups as $subgroup) {

                $subgroup2 = ContractUserGroup::find()->where(['contract_id' => $this->id, 'user_id' => $subgroup])->one();
                if (!$subgroup2) {
                    $up = true;
                    (new ContractUserGroup([
                        'contract_id' =>$this->id,
                        'user_id' => $subgroup
                    ]))->save(false);
                }
            }
            if($up == true and $this->isNewRecord == false){
                $a = ArrayHelper::getColumn($allSubs, 'user_id');
                $lost = ArrayHelper::getColumn(User::find()->where(['id' => $a])->all(), 'name');
                $a2 = ArrayHelper::getColumn(ContractUserGroup::find()->where(['contract_id' => $this->id])->all(), 'user_id');
                $new = ArrayHelper::getColumn(User::find()->where(['id' => $a2])->all(), 'name');
                $this->setToChangeTable('Группа пользователей: ',implode(', ', $lost),implode(', ', $new),'измнена');
            }
        }


        if($this->listFile != null){
            $files = MyUploadedFile::getInstancesByName('listFile', true);

            $files = array_combine(ArrayHelper::getColumn($this->listFile, 'name'), $files);
            foreach ($files as $name => $file){
                /** @var $file MyUploadedFile */

                $path = Yii::$app->security->generateRandomString();
                $path = "uploads/{$path}.$file->extension";

                $file->saveAs($path);

                $scan = new Scan([
                        'name' => $name,
                        'link' => $path,
                        'contract_id' => $this->id,
                    ]);
                $scan->save(false);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(ContractUserGroup::className(), ['contract_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }




    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Chat::className(), ['contract_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ContractQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContractQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['contract_id' => 'id']);
    }

    public function Change($last, $new)
    {
        $result = "";

//        if($last->author != $new->author) $result .= "Автор =>".$new->author->name."%0A";
        if($last->customer_id != $new->customer_id) $result .= 'Контрагент => '. $new->customer->name."%0A";
        if($last->name != $new->name) $result .= 'Название => '. $new->name."%0A";
        if($last->contract_number != $new->contract_number) $result .= 'Номер проекта => '. $new->contract_number."%0A";
        if($last->contract_date != $new->contract_date) $result .= 'Дата проекта => '. $new->contract_date."%0A";
        if($last->contract_status != $new->contract_status) $result .= 'Статус проекта => '. $new->contract_status."%0A";
        if($last->city_id != $new->city_id) $result .= 'Город => '. $new->city->name."%0A";
        if($last->contract_type != $new->contract_type) $result .= 'Вид проекта => '. $new->contract_type."%0A";
        if($last->project_name != $new->project_name) $result .= 'Название проекта => '. $new->project_name."%0A";
        if($last->contract_deadline != $new->contract_deadline) $result .= 'Срок окончания проекта => '. $new->contract_deadline."%0A";
        if($last->post_address != $new->post_address) $result .= 'Почтовый адрес => '. $new->post_address."%0A";
        if($last->comment != $new->comment) $result .= 'Комментарий => '. $new->comment."%0A";
        if($last->listfile != $new->listfaie) $result .= 'Загрузка документа => '. $new->file->name."%0A";
        /*'file' => 'Файл',
        'commentary' => 'Комментарии',*/
        return $result;
    }

    public function setChanging($old, $new, $status)
    {
        if($status == "Изменено"){

//            $this->setToChangeTable('Автор',"",$new->author->name,$status);
            $this->setToChangeTable('Контрагент',"",$new->customer_id->name,$status);
            $this->setToChangeTable('Название',"",$new->name,$status);
            $this->setToChangeTable('Номер проекта',"",$new->contract_number->name,$status);
            $this->setToChangeTable('Дата проекта',"",$new->contract_date,$status);
            $this->setToChangeTable('Статус проекта',"",$new->contract_status,$status);
            $this->setToChangeTable('Город',"",$new->city_id->name,$status);
            $this->setToChangeTable('Вид проекта',"",$new->contract_type,$status);
            $this->setToChangeTable('Название проекта',"",$new->project_name,$status);
            $this->setToChangeTable('Срок окончания проекта',"",$new->contract_deadline,$status);
            $this->setToChangeTable('Почтовый адрес',"",$new->post_address,$status);
            $this->setToChangeTable('Комментарий',"",$new->comment,$status);
            $this->setToChangeTable('Загрузка документа',"",$new->listfaie,$status);
        }else{
            if($old->contract_type != $new->contract_type)
                $this->setToChangeTable('Вид проекта',$old->contract_type,$new->contract_type,$status);
            if($old->author != $new->author)
//                $this->setToChangeTable('Автор',$old->author->name,$new->author->name,$status);
//            if($old->customer_id != $new->customer_id)
//                $this->setToChangeTable('Контрагент',$old->customer->name,$new->customer->name,$status);
            if($old->name != $new->name)
                $this->setToChangeTable('Название',$old->name,$new->name,$status);
            if($old->contract_number != $new->contract_number)
                $this->setToChangeTable('Номер проекта',$old->contract_number,$new->contract_number,$status);
            if($old->contract_date != $new->contract_date)
                $this->setToChangeTable('Дата проекта',$old->contract_date,$new->contract_date,$status);
            if($old->contract_status != $new->contract_status)
                $this->setToChangeTable('Статус проекта',$old->contract_status,$new->contract_status,$status);
//            if($old->city_id != $new->city_id)
//                $this->setToChangeTable('Город',$old->city->name,$new->city->name,$status);
            if($old->project_name != $new->project_name)
                $this->setToChangeTable('Название проекта',$old->project_name,$new->project_name,$status);
            if($old->contract_deadline != $new->contract_deadline)
                $this->setToChangeTable('Срок окончания проекта',$old->contract_deadline,$new->contract_deadline,$status);
            if($old->post_address != $new->post_address)
                $this->setToChangeTable('Почтовый адрес',$old->post_address,$new->post_address,$status);
            if($old->comment != $new->comment)
                $this->setToChangeTable('Комментарий',$old->comment,$new->comment,$status);
            if($old->listFile != $new->listFile) {
                $info = '';
                foreach ($new->listFile as $item) {
                    $info .= $item['name'].', ';
                }
                (new History([
                    'user_id' => Yii::$app->user->identity->id,
                    'contract_id' => $this->id,
                    'history_comment' => 'Были загружены документы: '.$info,
                    'created_at' => date('Y-m-d H:i:s'),
                ]))->save(false);
            }

        }

    }
    public function setToChangeTable($field,$old_value,$new_value,$status)
    {
        $model = new History();
        $model->contract_id = $this->id;
        $model->created_at = date('Y-m-d H:i:s');
        $model->user_id = Yii::$app->user->identity->id;
        $model->history_comment = "Были изменения в '{$field}' с '{$old_value}' на '{$new_value}'";

        $model->save();
    }

}
