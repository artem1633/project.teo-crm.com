<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work_type".
 *
 * @property int $id
 * @property string $name наименование типа работ
 *
 * @property Contract[] $contracts
 * @property WorkKind[] $workKinds
 */
class WorkType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Тип работ',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        $count = OrderWorkType::find()->where(['work_type_id' => $this->id])->count();
        $count += ContractToWorkType::find()->where(['work_type_id' => $this->id])->count();
        $count += TaskWorkType::find()->where(['work_type_id' => $this->id])->count();
        $count += WorkKind::find()->where(['work_type_id' => $this->id])->count();

        if($count > 0){
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContracts()
    {
        return $this->hasMany(Contract::className(), ['work_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkKinds()
    {
        return $this->hasMany(WorkKind::className(), ['work_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return WorkTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WorkTypeQuery(get_called_class());
    }
}
