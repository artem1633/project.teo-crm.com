<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task_history".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $task_id Id Задачи
 * @property string $history_comment Содержание действия
 * @property string $created_at
 *
 * @property Task $task
 * @property User $user
 */
class TaskHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'task_id'], 'integer'],
            [['history_comment'], 'string'],
            [['created_at'], 'safe'],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'task_id' => 'Id Задачи',
            'history_comment' => 'Содержание действия',
            'created_at' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return TaskHistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskHistoryQuery(get_called_class());
    }
}
