<?php

namespace app\models;

use app\components\MyUploadedFile;
use Codeception\Lib\Interfaces\ActiveRecord;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $name Название заказа
 * @property int $customer_id Наименоване организаци
 * @property int $author_id Автор
 * @property int $city_id Город

 * @property string $order_date Дата заказа
 * @property int $contract_id id договора
 * @property string $order_deadline Срок исполнения
 * @property integer $order_status_id Статус заказа
 * @property int $expired Просрочен
 * @property int $scan_added Добавлен ли скан
 * @property int $related_orders Есть ли связанные заказы
 * @property string $comment Комментарии
 * @property string $order_sum Место Сумма заказа
 * @property int $is_closed Закрыт

 * @property string $hours_plane часов план
 * @property int $hours_fact Часов факт
 *
 * @property User $author
 * @property City $city
 * @property Contract $contract
 * @property Customer $customer
 * @property Task[] $tasks
 */
class Order extends \yii\db\ActiveRecord
{

    public $listFile;
    public $file;

    public $userGroup;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'author_id', 'city_id', 'contract_id', 'expired', 'scan_added', 'related_orders', 'order_status_id', 'is_closed','hours_plane','hours_fact'], 'integer'],
            [['order_date', 'order_deadline', 'fact_end_date','created_at','updated_at','listFile','order_sum', 'userGroup'], 'safe'],
            [['comment'], 'string'],
            [['name','order_sum'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => null,
                'createdByAttribute' => 'author_id',
                'value' => Yii::$app->user->id
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название заказа',
            'customer_id' => 'Наименоване организаци',
            'author_id' => 'Автор',
            'city_id' => 'Город',
            'order_date' => 'Дата заказа',
            'contract_id' => 'Договор',
            'fact_end_date' =>'Дата фактического закртыия заказа',
            'order_deadline' => 'Срок исполнения',
            'order_status_id' => 'Статус заказа',
            'expired' => 'Просрочен',
            'scan_added' => 'Добавлен ли скан',
            'related_orders' => 'Есть ли связанные заказы',
            'userGroup' => 'Группа пользователей',
            'comment' => 'Комментарии',
            'hours_plane' => 'Часов план',
            'hours_fact' => 'Часов факт',
            'order_sum' => 'Сумма заказа',

            'is_closed' => 'Закрыт',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }
    public function beforeDelete()
    {
        $count = Contract::find()->where(['id' => $this->contract_id])->count();
        if($count > 0){
            return false;
        }
        OrderHistory::deleteAll(['order_id' => $this->id]);

        return parent::beforeDelete();
    }

    public function beforeSave($insert)
    {
        if (!$this->isNewRecord) {
            $last = Order::findOne($this->id);
            $this->setChanging($last, $this, false);
        }
        if (parent::beforeSave($insert)) {

            //... тут ваш код

            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert){
            (new OrderHistory([
                'user_id' => Yii::$app->user->identity->id,
                'order_id' => $this->id,
                'history_comment' => 'Был создан заказ',
                'created_at' => date('Y-m-d H:i:s'),
            ]))->save(false);
        }

        if(is_dir('uploads') == false){
            mkdir('uploads');
        }


        if($this->listFile != null) {
            $files = MyUploadedFile::getInstancesByName('listFile', true);

            $files = array_combine(ArrayHelper::getColumn($this->listFile, 'name'), $files);
            foreach ($files as $name => $file) {
                /** @var $file MyUploadedFile */

                $path = Yii::$app->security->generateRandomString();
                $path = "uploads/{$path}.$file->extension";

                $file->saveAs($path);

                $scan = new Scan([
                    'name' => $name,
                    'link' => $path,
                    'order_id' => $this->id,
                ]);
                $scan->save(false);
            }
        }

        OrderUserGroup::deleteAll(['order_id' => $this->id]);
        if($this->userGroup){
            foreach ($this->userGroup as $group){
                (new OrderUserGroup([
                    'order_id' => $this->id,
                    'user_id' => $group,
                ]))->save(false);
            }
        }
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        $now = time();
        $deadline = strtotime($this->order_deadline.' 00:00:00');
        $fact = strtotime($this->fact_end_date.' 00:00:00');

        if($fact == false){
            return $now > $deadline;
        } else {
            return $fact > $deadline;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderUserGroup()
    {
        return $this->hasMany(OrderUserGroup::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'order_status_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['order_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }

    public function Change($last, $new)
    {
        $result = "";

//        if($last->author != $new->author) $result .= "Автор =>".$new->author->name."%0A";
        if($last->customer_id != $new->customer_id) $result .= 'Контрагент => '. $new->customer->name."%0A";
        if($last->name != $new->name) $result .= 'Название => '. $new->name."%0A";
        if($last->hours_fact != $new->hours_fact) $result .= 'Часов факт => '. $new->hours_fact."%0A";
        if($last->hours_plane != $new->hours_plane) $result .= 'Часов план => '. $new->hours_plane."%0A";
        if($last->object_number != $new->object_number) $result .= 'Номер Объекта => '. $new->object_number."%0A";
        if($last->order_date != $new->order_date) $result .= 'Дата договра => '. $new->order_date."%0A";
        if($last->order_status_id != $new->order_status_id) $result .= 'Статус заказа => '. $new->order_status_id->name."%0A";
//        if($last->city_id != $new->city_id) $result .= 'Город => '. $new->city->name."%0A";

        if($last->contract_id != $new->contract_id) $result .= 'Договор => '. $new->contract_id."%0A";

        if($last->order_sum != $new->order_sum) $result .= 'Сумма договора с НДС => '. $new->order_sum."%0A";

        if($last->fact_end_date != $new->fact_end_date) $result .= 'Дата фактического закртыия заказа => '. $new->fact_end_date."%0A";
//
        if($last->expired != $new->expired) $result .= 'Просрочен => '. $new->expired."%0A";


        if($last->comment != $new->comment) $result .= 'Комментарий => '. $new->comment."%0A";
        if($last->listfile != $new->listfaie) $result .= 'Загрузка документа => '. $new->file->name."%0A";
        /*'file' => 'Файл',
        'commentary' => 'Комментарии',*/
        return $result;
    }

    public function setChanging($old, $new, $status)
    {
        if($status == "Изменено"){

//            $this->setToChangeTable('Автор',"",$new->author->name,$status);
            $this->setToChangeTable('Контрагент',"",$new->customer_id->name,$status);
            $this->setToChangeTable('Название',"",$new->name,$status);
            $this->setToChangeTable('Часов план',"",$new->hours_plane,$status);
            $this->setToChangeTable('Часов факт',"",$new->hours_fact,$status);

            $this->setToChangeTable('Дата заказа',"",$new->order_date,$status);
            $this->setToChangeTable('Статус заказа',"",$new->order_status_id->name,$status);
//            $this->setToChangeTable('Город',"",$new->city_id->name,$status);


            $this->setToChangeTable('Договор',"",$new->contract_id,$status);
            $this->setToChangeTable('Срок окончания заказа',"",$new->order_deadline,$status);
            $this->setToChangeTable('Сумма договора с НДС',"",$new->order_sum,$status);
            $this->setToChangeTable('Дата фактического закртыия заказа',"",$new->fact_end_date,$status);
            $this->setToChangeTable('Просрочен',"",$new->expired,$status);



            $this->setToChangeTable('Комментарий',"",$new->comment,$status);
            $this->setToChangeTable('Загрузка документа',"",$new->listfaie,$status);
        }else{
            if($old->name != $new->name)
                $this->setToChangeTable('Название',$old->name,$new->name,$status);
            if($old->hours_plane != $new->hours_plane)
                $this->setToChangeTable('Часов план',$old->hours_plane,$new->hours_plane,$status);
            if($old->hours_fact != $new->hours_fact)
                $this->setToChangeTable('Часоов факт',$old->hours_fact,$new->hours_fact,$status);
//            if($old->author != $new->author)
//                $this->setToChangeTable('Автор',$old->author->name,$new->author->name,$status);
            if($old->customer_id != $new->customer_id){
                $oldval = Customer::find()->where(['id' => $old->customer_id])->one();
                $newval = Customer::find()->where(['id' => $new->customer_id])->one();
                if ($oldval == null){
                    $this->setToChangeTable('Контрагент',' ',$newval->name,$status);
                }else{
                $this->setToChangeTable('Контрагент',$oldval->name,$newval->name,$status);
                }
            }
            if($old->contract_id != $new->contract_id)
                $this->setToChangeTable('Договор',$old->contract_id,$new->contract_id,$status);

            if($old->order_date != $new->order_date)
                $this->setToChangeTable('Дата заказа',$old->order_date,$new->order_date,$status);

            if($old->order_status_id != $new->order_status_id){
                $oldval = OrderStatus::find()->where(['id' => $old->order_status_id])->one();
                $newval = OrderStatus::find()->where(['id' => $new->order_status_id])->one();
                if ($oldval == null){
                    $this->setToChangeTable('Статус заказа',' ',$newval->name,$status);
                }else{
                $this->setToChangeTable('Статус заказа',$oldval->name,$newval->name,$status);
                }
            }
//            if($old->city_id != $new->city_id)
//                $this->setToChangeTable('Город',$old->city->name,$new->city->name,$status);

            if($old->order_deadline != $new->order_deadline)
                $this->setToChangeTable('Срок окончания заказа',$old->order_deadline,$new->order_deadline,$status);
            if($old->order_sum != $new->order_sum)
                $this->setToChangeTable('Сумма договора с НДС',$old->order_sum,$new->order_sum,$status);
            if($old->fact_end_date != $new->fact_end_date)
                $this->setToChangeTable('Дата фактического закртыия заказа',$old->fact_end_date,$new->fact_end_date,$status);
            if($old->expired != $new->expired)
                $this->setToChangeTable('Просрочен',$old->expired,$new->expired,$status);

            if($old->comment != $new->comment)
                $this->setToChangeTable('Комментарий',$old->comment,$new->comment,$status);
            if($old->listFile != $new->listFile) {
                $info = '';
                foreach ($new->listFile as $item) {
                    $info .= $item['name'].', ';
                }
                (new OrderHistory([
                    'user_id' => Yii::$app->user->identity->id,
                    'order_id' => $this->id,
                    'history_comment' => 'Были загружены документы: '.$info,
                    'created_at' => date('Y-m-d H:i:s'),
                ]))->save(false);
            }

        }

    }
    public function setToChangeTable($field,$old_value,$new_value,$status)
    {
        $model = new OrderHistory();
        $model->order_id = $this->id;
        $model->created_at = date('Y-m-d H:i:s');
        $model->user_id = Yii::$app->user->identity->id;
        $model->history_comment = "Были изменения в '{$field}' с '{$old_value}' на '{$new_value}'";

        $model->save();
    }

}
