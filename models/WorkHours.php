<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "work_hours".
 *
 * @property int $id
 * @property int $task_id Id задачи
 * @property int $hours_count Количество часов
 * @property string $work_done Выполнены работы
 * @property string $comment Комментарий
 * @property int $user_id Автор
 * @property string $created_at Дата и время создания
 *
 * @property User $user
 * @property Task $task
 */
class WorkHours extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work_hours';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'hours_count', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['work_done', 'comment'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => null,
                'createdByAttribute' => 'user_id',
                'value' => Yii::$app->user->id
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_id' => 'Id задачи',
            'hours_count' => 'Количество часов',
            'work_done' => 'Выполнены работы',
            'comment' => 'Комментарий',
            'user_id' => 'Автор',
            'created_at' => 'Дата и время создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @inheritdoc
     * @return WorkHoursQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WorkHoursQuery(get_called_class());
    }
}
