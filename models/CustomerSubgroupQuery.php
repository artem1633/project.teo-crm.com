<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CustomerSubgroup]].
 *
 * @see CustomerSubgroup
 */
class CustomerSubgroupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CustomerSubgroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CustomerSubgroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
