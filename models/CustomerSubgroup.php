<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_subgroup".
 *
 * @property int $id
 * @property string $name название подгуппы контрагента
 * @property int $customer_id
 *
 * @property Customer $customer
 */
class CustomerSubgroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_subgroup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'customer_id' => 'Customer ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @inheritdoc
     * @return CustomerSubgroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CustomerSubgroupQuery(get_called_class());
    }
}
