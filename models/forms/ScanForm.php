<?php

namespace app\models\forms;

use app\models\History;
use app\models\OrderHistory;
use app\models\TaskHistory;
use Yii;
use yii\base\Model;
use app\components\MyUploadedFile;
use yii\helpers\ArrayHelper;
use app\models\Scan;

/**
 * Class ScanForm
 * @package app\models\forms
 */
class ScanForm extends Model
{
    /**
     * @var array
     */
    public $listFile;

    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $contractId;
    /**
     * @var int
     */
    public $taskId;
    /**
     * @var int
     */
    public $orderId;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['listFile'], 'safe'],
            [['userId', 'contractId','taskId','orderId'], 'integer'],
        ];
    }

    /**
     * @return boolean
     */
    public function loadFile()
    {
        if($this->listFile != null){

            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $files = MyUploadedFile::getInstancesByName('listFile', true);

            $files = array_combine(ArrayHelper::getColumn($this->listFile, 'name'), $files);
            foreach ($files as $name => $file){
                /** @var $file MyUploadedFile */

                $path = Yii::$app->security->generateRandomString();
                $path = "uploads/{$path}.$file->extension";

                $file->saveAs($path);

                $scan = new Scan([
                    'name' => $name,
                    'link' => $path,
                    'contract_id' => $this->contractId,
                    'order_id' => $this->orderId,
                    'task_id' => $this->taskId,
                    'user_id' => $this->userId,
                    'author_id' => Yii::$app->user->getId(),
                ]);

                $scan->save(false);
            }

            if($this->contractId != null){
                $info = '';
                foreach ($this->listFile as $item) {
                    $info .= $item['name'].', ';
                }
                (new History([
                    'user_id' => Yii::$app->user->identity->id,
                    'contract_id' => $this->contractId,
                    'history_comment' => 'Были загружены документы: '.$info,
                    'created_at' => date('Y-m-d H:i:s'),
                ]))->save(false);
            }
            if($this->orderId != null){
                $info = '';
                foreach ($this->listFile as $item) {
                    $info .= $item['name'].', ';
                }
                (new OrderHistory([
                    'user_id' => Yii::$app->user->identity->id,
                    'order_id' => $this->orderId,
                    'history_comment' => 'Были загружены документы: '.$info,
                    'created_at' => date('Y-m-d H:i:s'),
                ]))->save(false);
            }
            if($this->taskId != null){
                $info = '';
                foreach ($this->listFile as $item) {
                    $info .= $item['name'].', ';
                }
                (new TaskHistory([
                    'user_id' => Yii::$app->user->identity->id,
                    'task_id' => $this->taskId,
                    'history_comment' => 'Были загружены документы: '.$info,
                    'created_at' => date('Y-m-d H:i:s'),
                ]))->save(false);
            }
        }

        return true;
    }
}