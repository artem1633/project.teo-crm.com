<?php

namespace app\models;

use Yii;
use yii\validators\RequiredValidator;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $name название организации
 * @property string $address адрес организации
 *
 * @property Contract[] $contracts
 * @property CustomerContact[] $customerContacts
 * @property CustomerSubgroup[] $customerSubgroups
 */
class Customer extends \yii\db\ActiveRecord
{

    public $subgroups;
    public $contacts;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'address'], 'string', 'max' => 255],
            [['subgroups',], 'each', 'rule' => ['string']],
            [['contacts',], 'validateСontacts',],
            [['name'], 'unique'],
            [['name','address'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название организации',
            'address' => 'Адрес',
            'contacts' => 'контакты',
            'subgroups' => 'Группа',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        $count = Contract::find()->where(['customer_id' => $this->id])->count();

        if($count > 0){
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->subgroups != null) {
            $allSubs = CustomerSubgroup::find()->where(['customer_id' => $this->id])->all();
            foreach ($allSubs as $sub) {
                if (array_search($sub, $this->subgroups) !== false) {
                    continue;
                } else {
                    $sub->delete();
                }
            }
            foreach ($this->subgroups as $subgroup) {
                $subgroup2 = CustomerSubgroup::find()->where(['name' => $subgroup, 'customer_id' => $this->id])->one();
                if (!$subgroup2) {
                    (new CustomerSubgroup([
                        'name' => $subgroup,
                        'customer_id' => $this->id
                    ]))->save(false);
                }
            }
        }

        $contactAll = CustomerContact::find()->where(['customer_id' => $this->id,])->all();
        foreach ($contactAll as $item1) {
            $a = false;
            if ($this->contacts == null) {
                $item1->delete();
                continue;
            }
            foreach ($this->contacts as $item3) {
                if ($item3['id'] == $item1->id) {
                    $a = true;
                }
                if (!$a) {
                    $item1->delete();
                    break;
                }
            }
        }
        if ($this->contacts != null) {
            foreach ($this->contacts as $item) {
                $contact = CustomerContact::find()->where(['id' => $item['id']])->one();

                if (!$contact) {
                    (new CustomerContact([
                        'name' => $item['name'],
                        'position' => $item['position'],
                        'phone' => $item['phone'],
                        'duty_phone' => $item['duty_phone'],
                        'email' => $item['email'],
                        'customer_id' => $this->id,
                    ]))->save(false);
                }else{
                    $contact->name = $item['name'];
                    $contact->position = $item['position'];
                    $contact->phone = $item['phone'];
                    $contact->duty_phone = $item['duty_phone'];
                    $contact->email = $item['email'];
                    $contact->save(false);
                }
            }

        }
    }


    /**
     * @param $attribute
     */
    public function validateСontacts($attribute)
    {
        foreach($this->$attribute as $index => $row) {
            if ($row['name'] == null) {
                $key = $attribute . '[' . $index . '][name]';
                $this->addError($key, 'Обязательное для заполненния');
            }
        }
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContracts()
    {
        return $this->hasMany(Contract::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerContacts()
    {
        return $this->hasMany(CustomerContact::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerSubgroups()
    {
        return $this->hasMany(CustomerSubgroup::className(), ['customer_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return CustomerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CustomerQuery(get_called_class());
    }
}
