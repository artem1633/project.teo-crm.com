<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property int $id
 * @property string $name название
 * @property int $contract_create создание договора
 * @property int $contract_update изменение  договора
 * @property int $contract_delete удаление договора
 * @property int $contract_view просмотр договора
 * @property int $contract_view_all просмотр всех договорв
 * @property int $order_create создание заказа
 * @property int $order_update изменение заказа
 * @property int $order_delete удаление заказа
 * @property int $order_view просмотр заказа
 * @property int $order_view_all просмотр всех заказов
 * @property int $task_create создание задачи
 * @property int $task_update изменение задачи
 * @property int $task_delete удаление задачи
 * @property int $task_view просмотр задачи
 * @property int $task_view_all спросмотр всех задач
 * @property int $directory_access доступ к справочникам
 * @property int $user_create создание пользователей
 * @property int $user_update редактирование пользователей
 * @property int $user_delete удаление пользователей
 * @property int $user_view просмотр пользователей
 * @property int $user_view_all просмотр всех пользователей
 * @property int $report_access доступ к отчетам
 *
 * @property User[] $users
 */
class Role extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contract_create', 'contract_update', 'contract_delete', 'contract_view', 'contract_view_all', 'order_create', 'order_update', 'order_delete', 'order_view', 'order_view_all', 'task_create', 'task_update', 'task_delete', 'task_view', 'task_view_all', 'directory_access', 'user_create', 'user_update', 'user_delete', 'user_view', 'user_view_all', 'report_access',
                'contract_view_sum', 'contract_view_customer', 'order_view_sum', 'task_sum', 'task_time', 'task_payment', 'task_responsible'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Роль',
            'contract_create' => 'Добавление договора',
            'contract_update' => 'Изменение договора',
            'contract_delete' => 'Удаление договора',
            'contract_view' => 'Просмотр договора',
            'contract_view_all' => 'Просмотр всех договоров',
            'contract_view_sum' => 'Просмотр суммы',
            'contract_view_customer' => 'Просмотр контрагента',
            'order_create' => 'Добавление заказа',
            'order_update' => 'Изменение заказа',
            'order_delete' => 'Удаление заказа',
            'order_view' => 'Просмотр заказа',
            'order_view_all' => 'Просмотр всех заказов',
            'order_view_sum' => 'Просмотр суммы',
            'task_create' => 'Добавление задачи',
            'task_update' => 'Изменение задачи',
            'task_delete' => 'Удаление задачи',
            'task_view' => 'Просмотр задачи',
            'task_view_all' => 'Просмотр всех задач',
            'task_sum' => 'Просмотр суммы',
            'task_time' => 'Просмотр времени',
            'task_payment' => 'Просмотр платежей',
            'task_responsible' => 'Просмотр исполнителя',
            'directory_access' => 'Доступ к справочникам',
            'user_create' => 'Добавление пользователя',
            'user_update' => 'Изменение пользователя',
            'user_delete' => 'Удаление пользователя',
            'user_view' => 'Просмотр пользователя',
            'user_view_all' => 'Просмотр всех пользователей',
            'report_access' => 'Доступ к отчетам',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        $count = User::find()->where(['role_id' => $this->id])->count();

        if($count > 0){
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['role_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return RQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RQuery(get_called_class());
    }
}
